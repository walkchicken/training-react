// Constant.
import { FETCH_TAG } from 'constants/actions/tag';

// Interface.
import { ICheckedInfo } from 'types/common';

interface IStates {
  isLoading: boolean;
  isError: boolean;
  data: ICheckedInfo[];
}

// Initial state fetch data tags
const initTagState = {
  isLoading: false,
  isError: false,
  data: [] as ICheckedInfo[],
};

// Tags reducer
const tagsReducer = (
  state: IStates,
  action: { type: string; data: ICheckedInfo[] },
) => {
  console.log(action.type);

  switch (action.type) {
    case FETCH_TAG.FETCHING:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case FETCH_TAG.SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      };
    case FETCH_TAG.ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};

export { tagsReducer, initTagState };
