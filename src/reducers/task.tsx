// Constant.
import { FETCH_TASK } from 'constants/actions/task';

// Interface.
import { ITask } from 'types/task';

interface IStates {
  isLoading: boolean;
  isError: boolean;
  data: ITask[];
}

// Initial state fetch data task
const initTaskState = {
  isLoading: false,
  isError: false,
  data: [] as ITask[],
};

// Task reducer
const taskReducer = (
  state: IStates,
  action: { type: string; data: ITask[] },
) => {
  switch (action.type) {
    case FETCH_TASK.FETCHING:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case FETCH_TASK.SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      };
    case FETCH_TASK.ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };

    // Handle reducer for add new task action
    case FETCH_TASK.ADDING:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_TASK.ADD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: [...state.data, action.data[0]],
      };
    case FETCH_TASK.ADD_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };

    // Handle reducer for update task action
    case FETCH_TASK.UPDATING:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_TASK.UPDATE_SUCCESS: {
      // If data is array then find index by name list.
      const idx = state.data.findIndex(
        (item: ITask) => item.id === action.data[0].id,
      );

      let currentData: ITask[] = [];

      // Current data.
      currentData = [...state.data];

      if (idx >= 0) {
        currentData[idx] = action.data[0];
      }

      return {
        ...state,
        isLoading: false,
        isError: false,
        data: currentData,
      };
    }
    case FETCH_TASK.UPDATE_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };

    // Handle reducer for remove users action
    case FETCH_TASK.REMOVING:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_TASK.REMOVE_SUCCESS:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: action.data,
      };
    case FETCH_TASK.REMOVE_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };
    case FETCH_TASK.UPDATING_COMMENT:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_TASK.UPDATE_COMMENT_SUCCESS: {
      // If data is array then find index by name list.
      const idx = state.data.findIndex(
        (item: ITask) => item.id === action.data[0].id,
      );

      let currentData: ITask[] = [];

      // Current data.
      currentData = [...state.data];

      if (idx >= 0) {
        currentData[idx] = action.data[0];
      }

      return {
        ...state,
        isLoading: false,
        isError: false,
        data: currentData,
      };
    }
    case FETCH_TASK.UPDATE_COMMENT_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };
    default:
      return state;
  }
};

export { taskReducer, initTaskState };
