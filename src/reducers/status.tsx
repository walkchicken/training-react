// Constant.
import { FETCH_STATUS } from 'constants/actions/status';

// Interface.
import { ICheckedInfo } from 'types/common';

interface IStates {
  isLoading: boolean;
  isError: boolean;
  data: ICheckedInfo[];
}

// Initial state fetch data status.
const initStatusState = {
  isLoading: false,
  isError: false,
  data: [] as ICheckedInfo[],
};

// Status reducer.
const statusReducer = (
  state: IStates,
  action: { type: string; data: ICheckedInfo[] },
) => {
  switch (action.type) {
    case FETCH_STATUS.FETCHING:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case FETCH_STATUS.SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      };
    case FETCH_STATUS.ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};

export { statusReducer, initStatusState };
