// Constant.
import { FETCH_COLOR } from 'constants/actions/color';

// Interface.
import { ICheckedInfo } from 'types/common';

interface IStates {
  isLoading: boolean;
  isError: boolean;
  data: ICheckedInfo[];
}

// Initial state fetch data color
const initColorTaskState = {
  isLoading: false,
  isError: false,
  data: [] as ICheckedInfo[],
};

// Color reducer
const colorTaskReducer = (
  state: IStates,
  action: { type: string; data: ICheckedInfo[] },
) => {
  switch (action.type) {
    case FETCH_COLOR.FETCHING:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case FETCH_COLOR.SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      };
    case FETCH_COLOR.ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};

export { colorTaskReducer, initColorTaskState };
