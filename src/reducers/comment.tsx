// Constant.
import { FETCH_COMMENT } from 'constants/actions/comment';

// Interface.
import { IComment } from 'types/task';

interface IStates {
  isLoading: boolean;
  isError: boolean;
  data: IComment[];
}

// Initial state list
const initCommentState = {
  isLoading: false,
  isError: false,
  data: [] as IComment[],
};

// List reducer
const commentReducer = (
  state: IStates,
  action: { type: string; data: IComment[] },
) => {
  switch (action.type) {
    case FETCH_COMMENT.FETCHING:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case FETCH_COMMENT.SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      };
    case FETCH_COMMENT.ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };

    // Handle reducer for add new comment action
    case FETCH_COMMENT.ADDING:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_COMMENT.ADD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: [...state.data, action.data[0]],
      };
    case FETCH_COMMENT.ADD_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };
    default:
      return state;
  }
};

export { commentReducer, initCommentState };
