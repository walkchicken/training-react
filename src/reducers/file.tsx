// Constant.
import { FETCH_FILE } from 'constants/actions/file';

// Interface.
import { IFile } from 'types/task';

interface IStates {
  isLoading: boolean;
  isError: boolean;
  data: IFile[];
}

// Initial state fetch data file
const initFileState = {
  isLoading: false,
  isError: false,
  data: [] as IFile[],
};

// FileReducer
const fileReducer = (
  state: IStates,
  action: { type: string; data: IFile[] },
) => {
  switch (action.type) {
    case FETCH_FILE.FETCHING:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case FETCH_FILE.SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      };
    case FETCH_FILE.ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };

    // Handle reducer for add new file action
    case FETCH_FILE.ADDING:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_FILE.ADD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: [...state.data, action.data[0]],
      };
    case FETCH_FILE.ADD_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };
    default:
      return state;
  }
};

export { fileReducer, initFileState };
