// Constant.
import { FETCH_USERS } from 'constants/actions/user';

// Interface.
import { IUser } from 'types/user';

interface IStates {
  isLoading: boolean;
  isError: boolean;
  data: IUser[];
}

// Initial state users
const initUserState = {
  isLoading: false,
  isError: false,
  data: [] as IUser[],
};

// Users reducer
const usersReducer = (
  state: IStates,
  action: { type: string; data: IUser[] },
) => {
  console.log(action.type);

  switch (action.type) {
    case FETCH_USERS.FETCHING:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case FETCH_USERS.SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      };
    case FETCH_USERS.ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      return state;
  }
};

export { usersReducer, initUserState };
