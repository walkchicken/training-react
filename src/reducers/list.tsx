// Constant.
import { FETCH_LIST, LABEL } from 'constants/actions/list';

// Interface.
import { IList } from 'types/list';

interface IStates {
  isLoading: boolean;
  isError: boolean;
  data: IList[];
}

// Initial state fetch data list
const initListState = {
  isLoading: false,
  isError: false,
  data: [] as IList[],
};

// List reducer
const listReducer = (
  state: IStates,
  action: { type: string; data: IList[] },
) => {
  switch (action.type) {
    case FETCH_LIST.FETCHING:
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case FETCH_LIST.SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      };
    case FETCH_LIST.ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };

    case FETCH_LIST.UPDATING:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_LIST.UPDATE_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      };
    case FETCH_LIST.UPDATE_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };
    case FETCH_LIST.UPDATING_ONLY_LIST:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_LIST.UPDATE_ONLY_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.data,
      };
    case FETCH_LIST.UPDATE_ONLY_LIST_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };

    // Handle reducer for add new task action
    case FETCH_LIST.UPDATING_BACKLOG:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_LIST.UPDATE_BACKLOG_SUCCESS: {
      // If data is array then find index by name list.
      const idx = state.data.length
        ? state.data.findIndex((item: IList) => item.name === LABEL.BACKLOG)
        : -1;

      let currentData: IList[] = [];

      // Current data.
      currentData = [...state.data];

      if (idx >= 0) {
        currentData[idx] = action.data[0];
      }

      return {
        ...state,
        isLoading: false,
        isError: false,
        data: currentData,
      };
    }
    case FETCH_LIST.UPDATE_BACKLOG_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };
    // Handle reducer for add new task action
    case FETCH_LIST.ADDING:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_LIST.ADD_SUCCESS: {
      // If data is array then find index by name list.
      const idx = state.data.length
        ? state.data.findIndex(
            (item: IList) => item.name === action.data[0].name,
          )
        : -1;

      let currentData: IList[] = [];

      // Current data.
      currentData = [...state.data];

      if (idx >= 0) {
        currentData[idx] = action.data[0];
      }

      return {
        ...state,
        isLoading: false,
        isError: false,
        data: currentData,
      };
    }
    case FETCH_LIST.ADD_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };
    // Handle reducer for remove users action
    case FETCH_LIST.REMOVING:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: state.data,
      };
    case FETCH_LIST.REMOVE_SUCCESS:
      return {
        ...state,
        isLoading: true,
        isError: false,
        data: action.data,
      };
    case FETCH_LIST.REMOVE_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: state.data,
      };
    default:
      return state;
  }
};

export { listReducer, initListState };
