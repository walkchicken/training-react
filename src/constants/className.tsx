import { LABEL } from './actions/list';

// Render style for list item.
export const listStyles = {
  [LABEL.BACKLOG]: 'list',
  [LABEL.IN_PROGRESS]: 'list list-inprogress',
  [LABEL.DONE]: 'list list-done',
};

// Render style for title in list item.
export const titleStyles = {
  [LABEL.BACKLOG]: 'list-backlog-title',
  [LABEL.IN_PROGRESS]: 'list-title',
  [LABEL.DONE]: 'list-title',
};

// Render style for icon in list item.
export const iconStyles = {
  [LABEL.BACKLOG]: 'icon-plus-circle mdi mdi-plus-circle-outline',
  [LABEL.IN_PROGRESS]: 'icon-chevron mdi mdi-chevron-left',
  [LABEL.DONE]: 'icon-chevron mdi mdi-chevron-right',
};
