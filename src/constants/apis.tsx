// Interface..
import IApi from 'types/api';

// Constants api with json-server
const API: IApi = {
  API_HOST: 'https://reactts-json-server.herokuapp.com',
  PATH_TAGS: '/tags',
  PATH_COLORS: '/colors',
  PATH_USERS: '/users',
  PATH_COMMENTS: '/comments',
  PATH_FILES: '/files',
  PATH_STATUSES: '/statuses',
  PATH_TASKS: '/tasks',
  PATH_LISTS: '/lists',
};

export { API };
