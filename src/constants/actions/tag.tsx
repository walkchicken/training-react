// Constant fetch tags.
export const FETCH_TAG = {
  SUCCESS: 'FETCH_TAG_SUCCESS',
  FETCHING: 'FETCHING_TAGS',
  ERROR: 'FETCH_TAG_ERROR',
};

export const LABEL_TAG = {
  WEBIX: 'Webix',
  JET: 'Jet',
  EASY: 'Easy',
  HARD: 'Hard',
  KABAN: 'Kanban',
  DOCS: 'Docs',
};
