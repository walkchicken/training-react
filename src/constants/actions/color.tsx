// Constant fetch all color task.
export const FETCH_COLOR = {
  SUCCESS: 'FETCH_COLOR_SUCCESS',
  FETCHING: 'FETCHING_ALL_COLORS',
  ERROR: 'FETCH_COLOR_ERROR',
};

export const COLOR = {
  SUCCESS: 'Normal',
  WARNING: 'Low',
  DANGER: 'Urgent',
};
