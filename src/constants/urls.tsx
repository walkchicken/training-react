// Constant.s
import { API } from 'constants/apis';

// Interface..
import IUrl from 'types/url';

// Constants api with json-server
const URL: IUrl = {
  API_BASE_URL: `${API.API_HOST}`,
  API_BASE_TAGS: `${API.API_HOST}${API.PATH_TAGS}`,
  API_BASE_COLORS: `${API.API_HOST}${API.PATH_COLORS}`,
  API_BASE_USERS: `${API.API_HOST}${API.PATH_USERS}`,
  API_BASE_COMMENTS: `${API.API_HOST}${API.PATH_COMMENTS}`,
  API_BASE_FILES: `${API.API_HOST}${API.PATH_FILES}`,
  API_BASE_STATUSES: `${API.API_HOST}${API.PATH_STATUSES}`,
  API_BASE_TASKS: `${API.API_HOST}${API.PATH_TASKS}`,
  API_BASE_LISTS: `${API.API_HOST}${API.PATH_LISTS}`,
};

export { URL };
