// Component.
import Home from 'pages/Home';
import { StoreProvider } from 'store';

const App = () => {
  return (
    <StoreProvider>
      <Home />
    </StoreProvider>
  );
};

export default App;
