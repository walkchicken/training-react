// Create type common with id and name.
export type IInfo = { id: number; name: string };

// Create type common by IInfo and add properties isChecked.
export interface ICheckedInfo extends IInfo {
  isChecked: boolean;
}

export type IResponse<T> = {
  isLoading: boolean;
  isError: boolean;
  data: T[];
};

// Create common dispatch type.
export type IDispatch<T> = React.Dispatch<{
  type: string;
  data: T[];
}>;
