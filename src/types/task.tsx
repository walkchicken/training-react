import { ICheckedInfo, IDispatch, IResponse } from 'types/common';
import { IList } from './list';
import { IUser } from './user';

// Create interface task
export interface ITask extends Pick<ICheckedInfo, 'id'> {
  title: string;
  tagId: number[];
  userId: number;
  colorId: number;
  statusId: number;
  fileId: number[];
  commentId: number[];
}

// Create interface file.
export interface IFile extends Pick<ICheckedInfo, 'id'> {
  file: File;
}

// Create interface comment
export interface IComment extends Pick<ICheckedInfo, 'id'> {
  comment: string;
  time: string;
  userId: number;
}

export interface IEditTask extends ITask {
  userInTask?: IUser;
  colorTask?: ICheckedInfo;
  statusTask?: ICheckedInfo;
  fileInTask?: IFile[];
  column?: IList;
  dataLists: IResponse<IList>;
  dataTasks: IResponse<ITask>;
  dataUsers: IResponse<IUser>;
  dataColors: IResponse<ICheckedInfo>;
  dataStatuses: IResponse<ICheckedInfo>;
  dispatchLists: IDispatch<IList>;
  dispatchTasks: IDispatch<ITask>;
}
