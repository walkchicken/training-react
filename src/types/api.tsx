// Interface. api
interface IApi {
  API_HOST: string;
  PATH_TAGS: string;
  PATH_COLORS: string;
  PATH_USERS: string;
  PATH_COMMENTS: string;
  PATH_FILES: string;
  PATH_STATUSES: string;
  PATH_TASKS: string;
  PATH_LISTS: string;
}

export default IApi;
