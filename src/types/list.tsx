import { IInfo } from 'types/common';

// Create interface for list extends from ICommon and add task id.
export interface IList extends IInfo {
  taskId: number[];
}
