import { ICheckedInfo } from 'types/common';

// Create interface for user
export interface IUser extends ICheckedInfo {
  avatar: string;
}
