// Interface. url
interface IUrl {
  API_BASE_URL: string;
  API_BASE_TAGS: string;
  API_BASE_COLORS: string;
  API_BASE_USERS: string;
  API_BASE_COMMENTS: string;
  API_BASE_FILES: string;
  API_BASE_STATUSES: string;
  API_BASE_TASKS: string;
  API_BASE_LISTS: string;
}

export default IUrl;
