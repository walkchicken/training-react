// Style.
import './header.scss';

// Clsx.
import clsx from 'clsx';

// Lib.
import { useContext, useState } from 'react';
import { Popover } from 'react-tiny-popover';

// Constant.
import { ICONS_LOGO } from 'constants/icons';

// Component.
import Button from 'components/Button';
import PopOverFilterUser from 'components/PopOver/FilterUser';
import PopOverFilterLanguage from 'components/PopOver/FilterLanguage';

// Store.
import { IStoreContext, StoreContext } from 'store';

const Header = () => {
  const { dataUsers, userSelected, setUserSelected } =
    useContext<IStoreContext>(StoreContext);
  const [toggleFilterUser, setToggleFilterUser] = useState(false);
  const [toggleFilterLanguage, setToggleFilterLanguage] = useState(false);
  const [userFilter, setUserFilter] = useState('Users');
  const [language, setLanguage] = useState('English');
  const [languageSelected, setLanguageSelected] = useState(1);

  const handleToggleFilterUser = () => {
    setToggleFilterUser(!toggleFilterUser);
    setToggleFilterLanguage(false);
  };

  const handleToggleFilterLanguage = () => {
    setToggleFilterLanguage(!toggleFilterLanguage);
    setToggleFilterUser(false);
  };

  return (
    <header className='header'>
      {/* Logo */}
      <div className='logo'>
        <img
          className='logo-image'
          src={ICONS_LOGO}
          width='58'
          height='53'
          alt='Kanban'
        />
        <label className='logo-title'>Kanban</label>
      </div>

      {/* Navbar*/}
      <ul className='navbar'>
        {/* Element handle open filter user form */}
        <Popover
          isOpen={toggleFilterUser}
          positions={['bottom', 'top', 'left', 'right']} // preferred positions by priority
          content={() => {
            return (
              <PopOverFilterUser
                dataUsers={dataUsers.data}
                onUserFilter={setUserFilter}
                userSelected={userSelected}
                onUserSelected={setUserSelected}
                onToggleFilterUser={setToggleFilterUser}
              />
            );
          }}
        >
          <li
            className='nav-item nav-item-filter'
            onClick={handleToggleFilterUser}
          >
            <span
              className={clsx('icon-multiple mdi mdi-account-multiple', {
                'item-info': toggleFilterUser,
              })}
            ></span>
            <span
              className={clsx('nav-item-content nav-item-content-user', {
                'item-info': toggleFilterUser,
              })}
            >
              {userFilter}
            </span>
            <span
              className={clsx('icon-menu-user mdi mdi-menu-down', {
                'item-info': toggleFilterUser,
              })}
            ></span>
          </li>
        </Popover>
        {/* Element handle open selected language form */}
        <Popover
          isOpen={toggleFilterLanguage}
          positions={['bottom', 'top', 'left', 'right']} // preferred positions by priority
          content={() => {
            return (
              <PopOverFilterLanguage
                onFilterLanguage={setLanguage}
                languageSelected={languageSelected}
                onLanguageSelected={setLanguageSelected}
                onToggleFilterLanguage={setToggleFilterLanguage}
              />
            );
          }}
        >
          <li
            className='nav-item nav-item-language'
            onClick={handleToggleFilterLanguage}
          >
            <span
              className={clsx('icon-earth mdi mdi-earth', {
                'item-info': toggleFilterLanguage,
              })}
            ></span>
            <span
              className={clsx('nav-item-content', {
                'item-info': toggleFilterLanguage,
              })}
            >
              {language}
            </span>
            <span
              className={clsx('icon-menu-language mdi mdi-menu-down', {
                'item-info': toggleFilterLanguage,
              })}
            ></span>
          </li>
        </Popover>
        <li className='nav-item-button'>
          <Button>
            <span className='icon-export mdi mdi-export'></span>
            Export
          </Button>
        </li>
      </ul>
    </header>
  );
};

export default Header;
