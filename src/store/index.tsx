// Lib.
import { createContext, useState } from 'react';
import { useReducer, ReactNode } from 'react';

// Interface.
import { ICheckedInfo, IDispatch, IResponse } from 'types/common';
import { IComment, IFile, ITask } from 'types/task';
import { IUser } from 'types/user';
import { IList } from 'types/list';

// Reducer.
import { colorTaskReducer, initColorTaskState } from 'reducers/color';
import { commentReducer, initCommentState } from 'reducers/comment';
import { fileReducer, initFileState } from 'reducers/file';
import { initStatusState, statusReducer } from 'reducers/status';
import { initTagState, tagsReducer } from 'reducers/tag';
import { initTaskState, taskReducer } from 'reducers/task';
import { initUserState, usersReducer } from 'reducers/user';
import { initListState, listReducer } from 'reducers/list';

// Create interface for store context.
export interface IStoreContext {
  dataUsers: IResponse<IUser>;
  dataLists: IResponse<IList>;
  dataTasks: IResponse<ITask>;
  dataTags: IResponse<ICheckedInfo>;
  dataStatuses: IResponse<ICheckedInfo>;
  dataFiles: IResponse<IFile>;
  dataComments: IResponse<IComment>;
  dataColors: IResponse<ICheckedInfo>;
  selectedTask: number;
  userSelected: number;
  dispatchUsers: IDispatch<IUser>;
  dispatchLists: IDispatch<IList>;
  dispatchTasks: IDispatch<ITask>;
  dispatchTags: IDispatch<ICheckedInfo>;
  dispatchStatuses: IDispatch<ICheckedInfo>;
  dispatchFiles: IDispatch<IFile>;
  dispatchComments: IDispatch<IComment>;
  dispatchColorTask: IDispatch<ICheckedInfo>;
  setSelectedTask: (selectedTask: number) => void;
  setUserSelected: (userSelected: number) => void;
}

interface IProps {
  children: ReactNode;
}

export const StoreContext = createContext<IStoreContext>({} as IStoreContext);

/**
 * Component provider task container
 * @param children is react node
 * @returns provider with valueStore task state and dispatch task
 */
export const StoreProvider: React.FC<IProps> = ({ children }: IProps) => {
  const [dataUsers, dispatchUsers] = useReducer(usersReducer, initUserState);
  const [dataLists, dispatchLists] = useReducer(listReducer, initListState);
  const [dataTasks, dispatchTasks] = useReducer(taskReducer, initTaskState);
  const [dataTags, dispatchTags] = useReducer(tagsReducer, initTagState);
  const [dataStatuses, dispatchStatuses] = useReducer(
    statusReducer,
    initStatusState,
  );
  const [dataFiles, dispatchFiles] = useReducer(fileReducer, initFileState);
  const [dataComments, dispatchComments] = useReducer(
    commentReducer,
    initCommentState,
  );
  const [dataColors, dispatchColorTask] = useReducer(
    colorTaskReducer,
    initColorTaskState,
  );

  // Create state for selected task in list tasks.
  const [selectedTask, setSelectedTask] = useState(0);

  // Create state for filter user.
  const [userSelected, setUserSelected] = useState(0);

  return (
    <StoreContext.Provider
      value={{
        dataUsers,
        dataLists,
        dataTasks,
        dataTags,
        dataStatuses,
        dataFiles,
        dataComments,
        dataColors,
        selectedTask,
        userSelected,
        dispatchUsers,
        dispatchLists,
        dispatchTasks,
        dispatchTags,
        dispatchStatuses,
        dispatchFiles,
        dispatchComments,
        dispatchColorTask,
        setSelectedTask,
        setUserSelected,
      }}
    >
      {children}
    </StoreContext.Provider>
  );
};
