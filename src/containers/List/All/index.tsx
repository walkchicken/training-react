// Style.
import './allList.scss';

// Lib.
import clsx from 'clsx';
import { memo, useCallback, useContext, useEffect, useState } from 'react';
import { IStoreContext, StoreContext } from 'store';
import {
  DragDropContext,
  Draggable,
  Droppable,
  DropResult,
} from 'react-beautiful-dnd';

// Constant.
import { FETCH_LIST, LABEL } from 'constants/actions/list';
import { FETCH_TASK } from 'constants/actions/task';

// Action.
import { addNewTask } from 'actions/task';
import { updateAllList, updateBacklogList, updateOnlyList } from 'actions/list';

// Interface.
import { IList } from 'types/list';
import { ITask, IEditTask } from 'types/task';
import { IUser } from 'types/user';

// Component.
import BacklogList from 'components/List/Backlog';
import InProgressAndDoneList from 'components/List/InProgressAndDone';
import Task from 'containers/Task';
import TestList from 'components/List/Test';
import FormEdit from 'containers/Form/Edit';

const AllList = () => {
  const {
    dataLists,
    dataTasks,
    dispatchLists,
    dispatchTasks,
    dataTags,
    userSelected,
  } = useContext<IStoreContext>(StoreContext);

  const [columns, setColumns] = useState([] as IList[]);

  const [popupFormEdit, setPopupFormEdit] = useState({
    isShow: false,
    editTask: {} as IEditTask,
  });

  useEffect(() => {
    setColumns(dataLists.data);
  }, [dataLists.data]);

  // Handle create new task in backlog list.
  const handleAddTask = (listBacklog: IList) => {
    const valueNewTask = {
      id: dataTasks.data.length + 1,
      title: 'Example',
      tagId: [],
      userId: 0,
      colorId: 0,
      statusId: 0,
      fileId: [],
      commentId: [],
    };

    addNewTask(
      dispatchTasks,
      FETCH_TASK.ADDING,
      FETCH_TASK.ADD_SUCCESS,
      FETCH_TASK.ADD_ERROR,
      valueNewTask,
    );

    const fetchUpdateDataBacklogList = async () => {
      return await updateBacklogList(
        dispatchLists,
        FETCH_LIST.UPDATING_BACKLOG,
        FETCH_LIST.UPDATE_BACKLOG_SUCCESS,
        FETCH_LIST.UPDATE_BACKLOG_ERROR,
        listBacklog,
        valueNewTask,
      );
    };

    fetchUpdateDataBacklogList();
  };

  // Handle drag end.
  const onDragEnd = (
    result: DropResult,
    columns: IList[],
    setColumns: (columns: IList[]) => void,
  ) => {
    if (!result.destination) {
      return;
    }

    const { source, destination } = result;

    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = columns[Number(source.droppableId)];
      const destColumn = columns[Number(destination.droppableId)];
      const sourceItems = [...sourceColumn.taskId];
      const destItems = [...destColumn.taskId];
      const [removed] = sourceItems.splice(source.index, 1);

      destItems.splice(destination.index, 0, removed);

      // Update column.
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...sourceColumn,
          taskId: sourceItems,
        },
        [destination.droppableId]: {
          ...destColumn,
          taskId: destItems,
        },
      });

      updateAllList(
        dispatchLists,
        FETCH_LIST.UPDATING,
        FETCH_LIST.UPDATE_SUCCESS,
        FETCH_LIST.UPDATE_ERROR,
        columns,
        source,
        sourceColumn,
        sourceItems,
        destination,
        destColumn,
        destItems,
      );
    } else {
      const column = columns[Number(source.droppableId)];
      const copiedItems = [...column.taskId];
      const [removed] = copiedItems.splice(source.index, 1);

      copiedItems.splice(destination.index, 0, removed);

      // Update column/
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...column,
          taskId: copiedItems,
        },
      });

      updateOnlyList(
        dispatchLists,
        FETCH_LIST.UPDATING_ONLY_LIST,
        FETCH_LIST.UPDATE_ONLY_LIST_SUCCESS,
        FETCH_LIST.UPDATE_ONLY_LIST_ERROR,
        columns,
        source,
        column,
        copiedItems,
      );
    }
  };

  const handleEdit = useCallback(
    (status: boolean, value?: IEditTask) =>
      setPopupFormEdit({
        isShow: status,
        editTask: value || ({} as IEditTask),
      }),
    [],
  );

  // Render item task in column when drag drop.
  const renderDragItem = (item: number, index: number, column: IList) => {
    const task = dataTasks.isError
      ? ({} as ITask)
      : dataTasks.data.find((task) => task.id === item);

    return (
      <Draggable key={item} draggableId={item.toString()} index={index}>
        {(provided) => {
          return (
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              style={{
                userSelect: 'none',
                ...provided.draggableProps.style,
              }}
            >
              {userSelected === 0 ? (
                <Task
                  color='info'
                  selected={false}
                  task={task ? task : ({} as ITask)}
                  column={column}
                  handleEdit={handleEdit}
                />
              ) : (
                userSelected === task?.userId && (
                  <Task
                    color='info'
                    selected={false}
                    task={task ? task : ({} as ITask)}
                    column={column}
                    handleEdit={handleEdit}
                  />
                )
              )}
            </div>
          );
        }}
      </Draggable>
    );
  };

  // Render column dropable.
  const renderDragColumn = (columnId: string, column: IList) => (
    <Droppable droppableId={columnId} key={columnId}>
      {(provided) => {
        return (
          <div
            {...provided.droppableProps}
            ref={provided.innerRef}
            className={clsx(
              column.id === 3 || column.id === 4
                ? 'list-test-group'
                : 'list-group',
            )}
          >
            {column.taskId.map((item, index) =>
              renderDragItem(item, index, column),
            )}
            {provided.placeholder}
          </div>
        );
      }}
    </Droppable>
  );

  // Render title in list column.
  const renderTitleColumn = (columnId: string, column: IList) => {
    const columnName = column.name;
    const columnContent = renderDragColumn(columnId, column);

    switch (columnName) {
      case LABEL.BACKLOG:
        return (
          <BacklogList
            key={columnId}
            columnId={columnId}
            column={column}
            columnName={columnName}
            columnContent={columnContent}
            handleAddTask={handleAddTask}
          />
        );

      case LABEL.IN_PROGRESS:
      case LABEL.DONE:
        return (
          <InProgressAndDoneList
            key={columnId}
            columnId={columnId}
            column={column}
            columnName={columnName}
            columnContent={columnContent}
          />
        );

      default:
        return (
          <TestList
            key={columnId}
            columnId={columnId}
            column={column}
            columnName={columnName}
            columnContent={columnContent}
          />
        );
    }
  };

  const { isShow, editTask } = popupFormEdit;
  const { userInTask, colorTask, statusTask } = editTask;

  // Render all list.
  return (
    <div className='all-list'>
      <DragDropContext
        onDragEnd={(result) => onDragEnd(result, columns, setColumns)}
      >
        {Object.entries(columns).map(([columnId, column]) =>
          renderTitleColumn(columnId, column),
        )}
      </DragDropContext>
      {isShow && (
        <FormEdit
          task={editTask}
          tags={dataTags.data}
          user={userInTask ? userInTask : ({} as IUser)}
          color={colorTask}
          status={statusTask}
          valuePopupEdit={{ popupFormEdit: isShow, handleEdit }}
        />
      )}
    </div>
  );
};

export default memo(AllList);
