import { ComponentStory, ComponentMeta } from '@storybook/react';
import './task.scss';

import Task from 'containers/Task';
import { StoreProvider } from 'store';

export default {
  title: 'Task',
  component: Task,
} as ComponentMeta<typeof Task>;

const Template: ComponentStory<typeof Task> = (args) => (
  <StoreProvider>
    <Task {...args} />
  </StoreProvider>
);

const TaskInfo = Template.bind({});
TaskInfo.args = {
  color: 'info',
  selected: false,
  task: {
    id: 1,
    title: 'International Metrics Orchestrator',
    tagId: [1, 2, 3],
    userId: 1,
    colorId: 1,
    statusId: 1,
    fileId: [2, 1, 3],
    commentId: [2],
  },
};

const TaskSuccess = Template.bind({});
TaskSuccess.args = {
  color: 'success',
  selected: false,
  task: {
    id: 1,
    title: 'International Metrics Orchestrator',
    tagId: [1, 2, 3],
    userId: 1,
    colorId: 1,
    statusId: 1,
    fileId: [2, 1, 3],
    commentId: [2],
  },
};

const TaskWarning = Template.bind({});
TaskWarning.args = {
  color: 'warning',
  selected: false,
  task: {
    id: 1,
    title: 'International Metrics Orchestrator',
    tagId: [1, 2, 3],
    userId: 1,
    colorId: 1,
    statusId: 1,
    fileId: [2, 1, 3],
    commentId: [2],
  },
};

const TaskDanger = Template.bind({});
TaskDanger.args = {
  color: 'danger',
  selected: false,
  task: {
    id: 1,
    title: 'International Metrics Orchestrator',
    tagId: [1, 2, 3],
    userId: 1,
    colorId: 1,
    statusId: 1,
    fileId: [2, 1, 3],
    commentId: [2],
  },
};

export { TaskInfo, TaskSuccess, TaskWarning, TaskDanger };
