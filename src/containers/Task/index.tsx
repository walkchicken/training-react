// Style.
import './task.scss';

// Clsx.
import clsx from 'clsx';

// Lib.
import {
  memo,
  useContext,
  useEffect,
  useRef,
  useState,
  useMemo,
  useCallback,
} from 'react';

// Constant.
import { COLOR } from 'constants/actions/color';

// Interface.
import { IEditTask, ITask } from 'types/task';
import { IUser } from 'types/user';

// Store.
import { IStoreContext, StoreContext } from 'store';

// Component.
import Count from 'components/Count';
import Tag from 'components/Tag';
import { IList } from 'types/list';

// Container.
import ShowSelectAvatar from 'components/Task/SelectAvatar';
import ShowComment from 'components/Task/Comment';
import ShowSetting from 'components/Task/Setting';

interface IProps {
  color: string;
  selected: boolean;
  task: ITask;
  column: IList;
  handleEdit: (isShow: boolean, task?: IEditTask) => void;
}

const Task = ({ color, task, column, handleEdit }: IProps) => {
  const {
    dataLists,
    dataTasks,
    dataTags,
    dataUsers,
    dataColors,
    dataStatuses,
    dataComments,
    dataFiles,
    selectedTask,
    setSelectedTask,
    dispatchLists,
    dispatchTasks,
    dispatchComments,
  } = useContext<IStoreContext>(StoreContext);
  const taskSelectedRef = useRef<HTMLDivElement>({} as HTMLDivElement);
  const [popUpAvatarUser, setPopupAvatarUser] = useState(false);
  const [userInTask, setUserInTask] = useState({} as IUser);
  const [togglePopOverSetting, setTogglePopOverSetting] = useState(false);
  const [togglePopOverComment, setTogglePopOverComment] = useState(false);

  useEffect(() => {
    const findUserInTask = dataUsers.data.find(
      (item) => item.id === task.userId,
    ) as IUser;

    setUserInTask(findUserInTask);
  }, [dataUsers.data, task.userId]);

  const tagsInTask = useMemo(
    () => dataTags.data.filter((item) => task.tagId.indexOf(item.id) >= 0),
    [dataTags.data, task.tagId],
  );

  const colorTask = useMemo(
    () => dataColors.data.find((item) => item.id === task.colorId),
    [dataColors.data, task.colorId],
  );

  const statusTask = useMemo(
    () => dataStatuses.data.find((item) => item.id === task.statusId),
    [dataStatuses.data, task.statusId],
  );

  const commentInTask = useMemo(
    () =>
      dataComments.data.filter((item) => task.commentId.indexOf(item.id) >= 0),
    [dataComments.data, task.commentId],
  );

  const fileInTask = useMemo(
    () => dataFiles.data.filter((item) => task.fileId.indexOf(item.id) >= 0),
    [dataFiles.data, task.fileId],
  );

  // Component render tag in task.
  const renderTag = () => {
    return tagsInTask.map((tag, index) => (
      <Tag key={`${tag.id}_${index}`} tag={tag} />
    ));
  };

  // Component handle selected task if click to task.
  const handleSelected = () => {
    const selectedTTaskId = Number(taskSelectedRef.current.id);

    setSelectedTask(selectedTTaskId);
  };

  // Handle popover avatar user.
  const handlePopOverAvatarUser = useCallback(() => {
    setPopupAvatarUser(!popUpAvatarUser);
    setTogglePopOverSetting(false);
    setTogglePopOverComment(false);
  }, [popUpAvatarUser]);

  // Handle popover setting.
  const handlePopOverSetting = useCallback(() => {
    setTogglePopOverSetting(!togglePopOverSetting);
    setPopupAvatarUser(false);
    setTogglePopOverComment(false);
  }, [togglePopOverSetting]);

  // Handle popover comment.
  const handlePopOverComment = useCallback(() => {
    setTogglePopOverComment(!togglePopOverComment);
    setPopupAvatarUser(false);
    setTogglePopOverSetting(false);
  }, [togglePopOverComment]);

  const handleFormEdit = () => {
    handleEdit(!false, {
      ...task,
      userInTask,
      colorTask,
      statusTask,
      fileInTask,
      column,
      dataLists,
      dataTasks,
      dataUsers,
      dataColors,
      dataStatuses,
      dispatchLists,
      dispatchTasks,
    });
    setPopupAvatarUser(false);
    setTogglePopOverSetting(false);
    setTogglePopOverComment(false);
  };

  return (
    <div
      ref={taskSelectedRef}
      onClick={handleSelected}
      onDoubleClick={handleFormEdit}
      className={clsx('task', `task-${color}`, {
        'task-selected': selectedTask === task.id,
        'task-success': colorTask?.name === COLOR.SUCCESS,
        'task-warning': colorTask?.name === COLOR.WARNING,
        'task-danger': colorTask?.name === COLOR.DANGER,
      })}
      id={`${task.id}`}
    >
      <div className='task-header'>
        <span className='task-header-title'>{task.title}</span>

        {/** Select avatar. */}
        <ShowSelectAvatar
          popUpAvatarUser={popUpAvatarUser}
          dataUsers={dataUsers.data}
          dispatchTasks={dispatchTasks}
          task={task}
          userInTask={userInTask}
          onUserInTask={setUserInTask}
          onPopupAvatarUser={setPopupAvatarUser}
          handlePopOverAvatarUser={handlePopOverAvatarUser}
          selectedTask={selectedTask}
        />
      </div>
      <div className='task-content'>
        <div>{renderTag()}</div>
        <div className='task-content-controls'>
          {fileInTask.length > 0 && (
            <span className='icon-task icon-file mdi mdi-file'>
              <Count countValue={fileInTask} />
            </span>
          )}

          {/** Comment. */}
          <ShowComment
            togglePopOverComment={togglePopOverComment}
            dataComments={dataComments.data}
            dispatchComments={dispatchComments}
            dataUsers={dataUsers.data}
            dispatchTasks={dispatchTasks}
            task={task}
            commentInTask={commentInTask}
            handlePopOverComment={handlePopOverComment}
            selectedTask={selectedTask}
          />

          {/** Setting. */}
          <ShowSetting
            togglePopOverSetting={togglePopOverSetting}
            task={task}
            column={column}
            userInTask={userInTask}
            colorTask={colorTask}
            statusTask={statusTask}
            fileInTask={fileInTask}
            dispatchLists={dispatchLists}
            dataLists={dataLists}
            dispatchTasks={dispatchTasks}
            dataTasks={dataTasks}
            handleEdit={handleEdit}
            setTogglePopOverSetting={setTogglePopOverSetting}
            handlePopOverSetting={handlePopOverSetting}
            dataUsers={dataUsers}
            dataColors={dataColors}
            dataStatuses={dataStatuses}
            selectedTask={selectedTask}
          />
        </div>
      </div>
    </div>
  );
};

/**
 * Component handle re-render task.
 * @param prevTask all props prev task.
 * @param nextTask all props next task.
 * @returns if true then not render task.
 */
const taskPropsAreEqual = (prevTask: IProps, nextTask: IProps) => {
  return prevTask.color === nextTask.color && prevTask.task === nextTask.task;
};

export default memo(Task, taskPropsAreEqual);
