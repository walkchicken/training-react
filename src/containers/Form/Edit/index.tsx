// Style.
import './formEdit.scss';

// Clsx.
import clsx from 'clsx';

// Lib.
import { useCallback, useContext, useEffect, useState, useRef } from 'react';

// Constant.
import { FETCH_TASK } from 'constants/actions/task';
import { FILE_SIZE } from 'constants/fileSize';
import { FETCH_FILE } from 'constants/actions/file';

// Action.
import { updateTask } from 'actions/task';
import { addNewFile } from 'actions/file';

// Interface..
import { IEditTask } from 'types/task';
import { ICheckedInfo } from 'types/common';
import { IUser } from 'types/user';

// Store.
import { IStoreContext, StoreContext } from 'store';

// Custom hook
import { useMultiselect } from 'hooks';

// Component.
import Button from 'components/Button';
import PopupDelete from 'components/Popup/Delete';
import TextArea from 'components/TextArea';
import FileInput from 'components/FileInput';
import FilePreview from 'components/FilePreview';

// Container.
import Dropdown from 'components/Dropdown';
import { DROPDOWN_MODE } from 'constants/mode';
import MultiSelect from 'components/MultiSelect';

interface IProps {
  task: IEditTask;
  tags: ICheckedInfo[];
  user: IUser;
  color: ICheckedInfo | undefined;
  status: ICheckedInfo | undefined;
  valuePopupEdit: {
    popupFormEdit: boolean;
    handleEdit: (popupFormEdit: boolean, value?: IEditTask) => void;
  };
}

export interface IFormTask {
  id?: number;
  title: string;
  tagId: number[];
  userId: number;
  colorId: number;
  statusId: number;
  fileId: number[];
  commentId: number[];
}

const FormEdit = ({
  task,
  tags,
  user,
  color,
  status,
  valuePopupEdit,
}: IProps) => {
  const [popUpProcess, setPopUpProcess] = useState(false);
  const [popOverAssignUser, setPopOverAssignUser] = useState(false);
  const [popOverSelectColor, setPopOverSelectColor] = useState(false);
  const [popOverSelectStatus, setPopOverSelectStatus] = useState(false);
  const [popOverTag, setPopOverTag] = useState(false);
  const [userAssign, setUserAssign] = useState(
    user.name != undefined ? user.name : '',
  );
  const [selectUserId, setSelectUserId] = useState(0);
  const [selectColor, setSelectColor] = useState(
    color !== undefined ? color.name : '',
  );
  const [selectColorId, setSelectColorId] = useState(0);
  const [selectStatus, setSelectStatus] = useState(
    status !== undefined ? status.name : '',
  );
  const [selectStatusId, setSelectStatusId] = useState(0);
  const [valueTextArea, setValueTextArea] = useState(task.title);
  const [valueTagId, setValueTagId] = useState(task.tagId);
  const {
    selectedTagId,
    setSelectedTagId,
    isSelectedTagId,
    onChangeSelectedTagId,
    onClickDeleteTagId,
  } = useMultiselect([]);
  const inputUploadFileRef = useRef<HTMLInputElement>({} as HTMLInputElement);
  const [valueFile, setValueFile] = useState<File[]>([]);
  const { dispatchFiles } = useContext<IStoreContext>(StoreContext);

  // Close on Esc key.
  const closeOnEscapeKeyDown = useCallback(
    (event: { charCode: any; keyCode: any }) => {
      if ((event.charCode || event.keyCode) === 27) {
        valuePopupEdit.handleEdit(false);
      }
    },
    [valuePopupEdit],
  );

  useEffect(() => {
    const file = task.fileInTask?.map(
      (objectFile) => objectFile.file,
    ) as File[];

    setValueFile(file);
  }, [task.fileInTask]);

  useEffect(() => {
    setValueTextArea(task.title);
  }, [task.title]);

  useEffect(() => {
    setValueTagId(task.tagId);
  }, [task.tagId]);

  useEffect(() => {
    setSelectUserId(task.userId);
  }, [task.userId]);

  useEffect(() => {
    setSelectColorId(task.colorId);
  }, [task.colorId]);

  useEffect(() => {
    setSelectStatusId(task.statusId);
  }, [task.statusId]);

  useEffect(() => {
    document.body.addEventListener('keydown', closeOnEscapeKeyDown);
    return function cleanup() {
      document.body.removeEventListener('keydown', closeOnEscapeKeyDown);
    };
  }, [closeOnEscapeKeyDown]);

  useEffect(() => {
    // Filter tags in task b id.
    const tagsInTask = tags.filter((item) => valueTagId.indexOf(item.id) >= 0);

    /**
     * @return array tag name.
     */
    const tagSelected = tagsInTask.map((item) => {
      selectedTagId.push(item.id);
      return item.id;
    });

    // Set tag selected to state.
    setSelectedTagId(tagSelected);
  }, []);

  // Handle close popup process delete.
  const handleClosePopupProcess = () => {
    setPopUpProcess(!popUpProcess);
  };

  // Handle close form edit.
  const handleCloseFormEdit = () => {
    valuePopupEdit.handleEdit(false);
  };

  // Handle open | close popup assign user.
  const handlePopOverAssignUser = useCallback(() => {
    setPopOverAssignUser(!popOverAssignUser);
    setPopOverSelectColor(false);
    setPopOverSelectStatus(false);
    setPopOverTag(false);
  }, [popOverAssignUser]);

  // Handle open | close popup select color.
  const handlePopOverSelectColor = useCallback(() => {
    setPopOverSelectColor(!popOverSelectColor);
    setPopOverAssignUser(false);
    setPopOverSelectStatus(false);
    setPopOverTag(false);
  }, [popOverSelectColor]);

  // Handle open | close popup select status.
  const handlePopOverSelectStatus = useCallback(() => {
    setPopOverSelectStatus(!popOverSelectStatus);
    setPopOverSelectColor(false);
    setPopOverAssignUser(false);
    setPopOverTag(false);
  }, [popOverSelectStatus]);

  // Handle submit new value in task.
  const handleSubmitFormTask = () => {
    const dates = Number(new Date().getTime());

    if (valueFile.length) {
      valueFile.forEach((file, index) => {
        addNewFile(
          dispatchFiles,
          FETCH_FILE.ADDING,
          FETCH_FILE.ADD_SUCCESS,
          FETCH_FILE.ADD_ERROR,
          file,
          dates,
        );
      });
    }

    const valuesFileId = valueFile.map(
      (object) => object.lastModified + dates,
    ) as number[];

    const valueEditForm = {
      id: task.id,
      title: valueTextArea,
      tagId: selectedTagId,
      userId: selectUserId,
      colorId: selectColorId,
      statusId: selectStatusId,
      fileId: [...valuesFileId],
      commentId: task.commentId,
    };

    // Call to action update task.
    const fetchUpdateTask = async () => {
      return await updateTask(
        task.dispatchTasks,
        FETCH_TASK.UPDATING,
        FETCH_TASK.UPDATE_SUCCESS,
        FETCH_TASK.UPDATE_ERROR,
        valueEditForm,
      );
    };

    fetchUpdateTask();

    valuePopupEdit.handleEdit(false);
  };

  const handleUploadFile = () => {
    inputUploadFileRef.current.click();
  };

  const handleChangeUploadFile = (value: File) => {
    setValueFile(valueFile.concat(value));
  };

  const handleRemoveFileUpload = (nameFile: string) => {
    const validFileIndex = valueFile.findIndex((e) => e.name === nameFile);
    valueFile.splice(validFileIndex, 1);
    // update validFiles array
    setValueFile([...valueFile]);
  };

  return (
    <div
      className={clsx('form-edit-wrapper', {
        'form-edit-wrapper-show': valuePopupEdit.popupFormEdit,
      })}
      onClick={handleCloseFormEdit}
    >
      <div
        className='form-edit-container'
        onClick={(event) => event.stopPropagation()}
      >
        <div className='form-edit-header'>
          <span className='form-edit-title'>Edit card</span>
          <div className='button-close'>
            {/* Button close form edit. */}
            <Button primary={true} icon={true} onClick={handleCloseFormEdit}>
              <span className='icon-close mdi mdi-close'></span>
            </Button>
          </div>
        </div>
        <div className='form-edit-content'>
          <div className='form-edit-texts'>
            <label className='form-edit-label'>Text</label>
            <div className='form-edit-textarea'>
              {/* Title task. */}
              <TextArea
                valueTextArea={valueTextArea}
                onSetValueTextArea={setValueTextArea}
              />
            </div>
          </div>
          <div className='form-edit-tags'>
            <label className='form-edit-label'>Tags</label>
            <div className='form-edit-input'>
              <MultiSelect
                selectedTagId={selectedTagId}
                valuePopupEdit={valuePopupEdit}
                onClickDeleteTagId={onClickDeleteTagId}
                tags={tags}
                valueTagId={valueTagId}
                onSetValueTagId={setValueTagId}
                isSelectedTagId={isSelectedTagId}
                onChangeSelectedTagId={onChangeSelectedTagId}
                popOverTag={popOverTag}
                setPopOverTag={setPopOverTag}
                onPopOverAssignUser={setPopOverAssignUser}
                onPopOverSelectColor={setPopOverSelectColor}
                onPopOverSelectStatus={setPopOverSelectStatus}
              />
            </div>
          </div>

          <div className='form-edit-combobox'>
            <div className='form-edit-assign form-combobox'>
              <label className='form-edit-label'>Assign to</label>
              <div className='form-combobox-input'>
                <Dropdown
                  mode={DROPDOWN_MODE.USER}
                  dataDropdown={task.dataUsers.data}
                  valueSelect={userAssign}
                  onValueSelected={setUserAssign}
                  onSelectedId={setSelectUserId}
                  onPopOverSelected={setPopOverAssignUser}
                  popOverDropdown={popOverAssignUser}
                  handlePopOverDropdown={handlePopOverAssignUser}
                />
              </div>
            </div>
            <div className='form-edit-color form-combobox'>
              <label className='form-edit-label'>Color</label>
              <div className='form-combobox-input'>
                <Dropdown
                  mode={DROPDOWN_MODE.COLOR}
                  dataDropdown={task.dataColors.data}
                  valueSelect={selectColor}
                  onValueSelected={setSelectColor}
                  onSelectedId={setSelectColorId}
                  onPopOverSelected={setPopOverSelectColor}
                  popOverDropdown={popOverSelectColor}
                  handlePopOverDropdown={handlePopOverSelectColor}
                />
              </div>
            </div>
            <div className='form-edit-status form-combobox'>
              <label className='form-edit-label'>Status</label>
              <div className='form-combobox-input'>
                <Dropdown
                  mode={DROPDOWN_MODE.STATUS}
                  dataDropdown={task.dataStatuses.data}
                  valueSelect={selectStatus}
                  onValueSelected={setSelectStatus}
                  onSelectedId={setSelectStatusId}
                  onPopOverSelected={setPopOverSelectStatus}
                  popOverDropdown={popOverSelectStatus}
                  handlePopOverDropdown={handlePopOverSelectStatus}
                />
              </div>
            </div>
          </div>

          <div className='form-edit-file'>
            <div className='form-edit-file-upload'>
              <label className='form-edit-label'>Attachments</label>
              <FileInput
                inputUploadFileRef={inputUploadFileRef}
                onValueFile={handleChangeUploadFile}
              />
              {valueFile.length < FILE_SIZE && (
                <div className='button-upload'>
                  {/* Button upload file. */}
                  <Button
                    primary={true}
                    icon={true}
                    hover={true}
                    onClick={handleUploadFile}
                  >
                    <span className='icon-upload mdi mdi-upload' />
                    <span className='button-upload'>Upload</span>
                  </Button>
                </div>
              )}
            </div>
            {valueFile.length ? (
              <div className='preview-file'>
                {valueFile.map((file, index) => (
                  <FilePreview
                    key={`${file.name}_${index}`}
                    file={file}
                    onRemoveFileUpload={handleRemoveFileUpload}
                  />
                ))}
              </div>
            ) : (
              <div className='form-edit-file-drop'>
                <div className='drop'>
                  <span className='drop-content'>Drop Files Here</span>
                </div>
              </div>
            )}
          </div>
        </div>

        <div className='form-edit-footer'>
          <div className='button-remove'>
            {/* Button remove task. */}
            <Button
              primary={true}
              icon={false}
              onClick={handleClosePopupProcess}
            >
              Remove
            </Button>
          </div>

          {/* Popup process delete task. */}
          {popUpProcess && (
            <PopupDelete
              onPopUpProcess={setPopUpProcess}
              task={task}
              valuePopupEdit={valuePopupEdit}
            />
          )}
          <div className='button-save'>
            {/* Button save update task. */}
            <Button primary={true} icon={false} onClick={handleSubmitFormTask}>
              Save
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormEdit;
