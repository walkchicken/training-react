import { AxiosResponse } from 'axios';
import { IList } from 'types/list';

/**
 * Format all list with result dragdrop.
 * @param columns List array default.
 * @param sourceResult Result new list drag.
 * @param destinationResult Result new list drop.
 * @returns New List array.
 */
export const formatDataAllList = (
  columns: IList[],
  sourceResult: AxiosResponse<any, any>,
  destinationResult: AxiosResponse<any, any>,
) => {
  const idxSource = columns.findIndex((obj) => obj.id === sourceResult.data.id);
  const idxDestination = columns.findIndex(
    (obj) => obj.id === destinationResult.data.id,
  );

  let currentData: IList[] = [];

  // current data.
  currentData = [...columns];

  if (idxSource >= 0 && idxDestination >= 0) {
    currentData[idxSource].taskId = sourceResult.data.taskId;
    currentData[idxDestination].taskId = destinationResult.data.taskId;
  }

  return currentData;
};

/**
 * Format only list with result dragdrop.
 * @param columns List array default.
 * @param sourceResult Result drag and drop.
 * @returns New list array.
 */
export const formatDataOnlyList = (
  columns: IList[],
  sourceResult: AxiosResponse<any, any>,
) => {
  const idxSource = columns.findIndex((obj) => obj.id === sourceResult.data.id);

  let currentData: IList[] = [];
  // current data.
  currentData = [...columns];

  if (idxSource >= 0) {
    currentData[idxSource].taskId = sourceResult.data.taskId;
  }

  return currentData;
};
