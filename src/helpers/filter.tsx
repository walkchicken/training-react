// Interface.
import { IList } from 'types/list';
import { ITask } from 'types/task';

/**
 * Component helper filter name in array
 * @param data
 * @param name
 * @returns filtered data
 */
export const dataFilterByName = (data: IList[], name: string) => {
  return data.filter((item) => item.name === name);
};

/**
 * Component helper filter ids in array data.taskId
 * @param data
 * @param dataIds
 * @returns filtered data
 */
export const dataFilterById = (data: IList, dataIds: ITask[]) => {
  return dataIds.filter((item) => data.taskId.indexOf(item.id) >= 0);
};

export const filterIdRemove = (column: IList, taskIdRemove: number) => {
  const index = column.taskId.indexOf(taskIdRemove);
  if (index > -1) {
    // only splice array when item is found
    column.taskId.splice(index, 1); // 2nd parameter means remove one item only
  }

  return column.taskId;
};

export const filterRemoveTask = (dataTasks: ITask[], taskIdRemove: number) => {
  return dataTasks.filter((task) => task.id !== taskIdRemove);
};
