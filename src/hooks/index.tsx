// Lib.
import React, { useState } from 'react';

/**
 * Custom hook for multiselect.
 * @param initialValue is number[].
 * @returns props
 * selectedTagId,
 * setSelectedTagId,
 * isSelectedTagId,
 * onChangeSelectedTagId,
 * onClickDeleteTagId
 */
export const useMultiselect = (initialValue: number[]) => {
  const [selectedTagId, setSelectedTagId] = useState<number[]>(initialValue);

  const onChangeSelectedTagId = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    const value = Number(event.target.value);
    const index = selectedTagId.indexOf(value);
    if (index > -1) {
      setSelectedTagId([
        ...selectedTagId.slice(0, index),
        ...selectedTagId.slice(index + 1),
      ]);
    } else {
      setSelectedTagId([...selectedTagId, ...[value]]);
    }
  };

  const onClickDeleteTagId = (valueDelete: number) => {
    const index = selectedTagId.indexOf(valueDelete);
    if (index > -1) {
      setSelectedTagId([
        ...selectedTagId.slice(0, index),
        ...selectedTagId.slice(index + 1),
      ]);
    } else {
      setSelectedTagId([...selectedTagId, ...[valueDelete]]);
    }
  };

  const isSelectedTagId = (value: number) => {
    return selectedTagId.includes(value);
  };

  return {
    selectedTagId,
    setSelectedTagId,
    isSelectedTagId,
    onChangeSelectedTagId,
    onClickDeleteTagId,
  };
};
