// Style.
import './home.scss';

// Lib.
import { useContext, useEffect } from 'react';

// Action.
import { getDataUser } from 'actions/user';
import { getDataAllList } from 'actions/list';
import { getDataAllTask } from 'actions/task';
import { getDataAllTag } from 'actions/tag';
import { getDataAllStatus } from 'actions/status';
import { getDataAllColorTask } from 'actions/color';
import { getDataAllComment } from 'actions/comment';
import { getDataAllFile } from 'actions/file';

// Constant.
import { FETCH_USERS } from 'constants/actions/user';
import { FETCH_TASK } from 'constants/actions/task';
import { FETCH_LIST } from 'constants/actions/list';
import { FETCH_TAG } from 'constants/actions/tag';
import { FETCH_COLOR } from 'constants/actions/color';
import { FETCH_STATUS } from 'constants/actions/status';
import { FETCH_COMMENT } from 'constants/actions/comment';
import { FETCH_FILE } from 'constants/actions/file';

// Store.
import { IStoreContext, StoreContext } from 'store';

// Component.
import Header from 'layout/Header';
import AllList from 'containers/List/All';

const Home = () => {
  const {
    dispatchUsers,
    dispatchLists,
    dispatchTasks,
    dispatchTags,
    dispatchColorTask,
    dispatchStatuses,
    dispatchComments,
    dispatchFiles,
  } = useContext<IStoreContext>(StoreContext);

  // Create useEffect call to getDataUser with dispatch users.
  useEffect(() => {
    // getDataUser with dispatch users.
    getDataUser(
      dispatchUsers,
      FETCH_USERS.FETCHING,
      FETCH_USERS.SUCCESS,
      FETCH_USERS.ERROR,
    );

    // getDataAllList with dispatch lists.
    getDataAllList(
      dispatchLists,
      FETCH_LIST.FETCHING,
      FETCH_LIST.SUCCESS,
      FETCH_LIST.ERROR,
    );

    // Declare the data fetching all task function.
    const fetchDataAllTask = async () => {
      // getDataAllTask with dispatch tasks.
      return await getDataAllTask(
        dispatchTasks,
        FETCH_TASK.FETCHING,
        FETCH_TASK.SUCCESS,
        FETCH_TASK.ERROR,
      );
    };

    // Declare the data fetching all tag function.
    const fetchDataAllTag = async () => {
      // getDataAllTag with dispatch tags.
      return await getDataAllTag(
        dispatchTags,
        FETCH_TAG.FETCHING,
        FETCH_TAG.SUCCESS,
        FETCH_TAG.ERROR,
      );
    };

    // Declare the data fetching all status function.
    const fetchDataAllColorTask = async () => {
      // getDataAllColorTask with dispatch status.
      return await getDataAllColorTask(
        dispatchColorTask,
        FETCH_COLOR.FETCHING,
        FETCH_COLOR.SUCCESS,
        FETCH_COLOR.ERROR,
      );
    };

    // Declare the data fetching all status function.
    const fetchDataAllStatus = async () => {
      // getDataAllStatus with dispatch status.
      return await getDataAllStatus(
        dispatchStatuses,
        FETCH_STATUS.FETCHING,
        FETCH_STATUS.SUCCESS,
        FETCH_STATUS.ERROR,
      );
    };

    // Declare the data fetching all comment function.
    const fetchDataAllComment = async () => {
      // getDataAllComment with dispatch comment.
      return await getDataAllComment(
        dispatchComments,
        FETCH_COMMENT.FETCHING,
        FETCH_COMMENT.SUCCESS,
        FETCH_COMMENT.ERROR,
      );
    };

    // Declare the data fetching all file function.
    const fetchDataAllFile = async () => {
      // getDataAllFile with dispatch file.
      return await getDataAllFile(
        dispatchFiles,
        FETCH_FILE.FETCHING,
        FETCH_FILE.SUCCESS,
        FETCH_FILE.ERROR,
      );
    };

    // Call function fetch all task
    fetchDataAllTask();

    // Call function fetch all tag
    fetchDataAllTag();

    // Call function fetch all color
    fetchDataAllColorTask();

    // Call function fetch all status
    fetchDataAllStatus();

    fetchDataAllComment();

    fetchDataAllFile();
  }, []);

  return (
    <div className='wrapper'>
      <Header />
      <div className='container'>
        {/* Call AllList component to render all list in the page */}
        <AllList />
      </div>
    </div>
  );
};

export default Home;
