// Style.
import './avatar.scss';

// Lib.
import clsx from 'clsx';

// Interface.
import { IUser } from 'types/user';
import { memo } from 'react';

interface IProps {
  size: boolean;
  user: IUser;
  styleChecked?: boolean;
}

const Avatar = ({ size, user, styleChecked }: IProps) => {
  const checkedSize = size ? 'avatar-normal' : 'avatar-small';
  return (
    <>
      {user ? (
        <img
          className={clsx('avatar', checkedSize, {
            'avatar-checked': styleChecked,
          })}
          src={user.avatar}
          alt={`avatar-${user.name}`}
        />
      ) : (
        <span className='icon-account mdi mdi-account'></span>
      )}
    </>
  );
};

const avatarPropsAreEqual = (prevAvatar: IProps, nextAvatar: IProps) => {
  return (
    prevAvatar.size === nextAvatar.size && prevAvatar.user === nextAvatar.user
  );
};

export default memo(Avatar, avatarPropsAreEqual);
