import { ComponentStory, ComponentMeta } from '@storybook/react';
import './avatar.scss';

import Avatar from './index';

export default {
  title: 'Avatar',
  component: Avatar,
} as ComponentMeta<typeof Avatar>;

const Template: ComponentStory<typeof Avatar> = (args) => <Avatar {...args} />;

const AvatarNormal = Template.bind({});
AvatarNormal.args = {
  size: true,
  user: {
    id: 1,
    name: 'Jayce Crist',
    avatar:
      'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/851.jpg',
    isChecked: true,
  },
};

const AvatarSmall = Template.bind({});
AvatarSmall.args = {
  size: false,
  user: {
    id: 1,
    name: 'Jayce Crist',
    avatar:
      'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/851.jpg',
    isChecked: true,
  },
};

const AvatarStyleChecked = Template.bind({});
AvatarStyleChecked.args = {
  size: false,
  user: {
    id: 1,
    name: 'Jayce Crist',
    avatar:
      'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/851.jpg',
    isChecked: true,
  },
  styleChecked: true,
};

export { AvatarNormal, AvatarSmall, AvatarStyleChecked };
