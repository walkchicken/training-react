// Style.
import './dropdown.scss';

// Clsx.
import clsx from 'clsx';

// Lib.
import { memo } from 'react';

// Interface.
import { IUser } from 'types/user';
import { ICheckedInfo } from 'types/common';
import { DROPDOWN_MODE } from 'constants/mode';
import { COLOR } from 'constants/actions/color';

interface IStates {
  mode: string;
  item: IUser | ICheckedInfo;
  valueSelect?: string;
  onValueSelected: (valueSelected: string) => void;
  onSelectedId: (selectedId: number) => void;
  onPopOverSelected: (popOverSelected: boolean) => void;
}

/**
 * @param user is data from renderUser and type of IUser
 * @param setUserAssign is handle selected name user assign
 * @returns JSX element li with data user
 */
const DropdownItem = ({
  mode,
  item,
  valueSelect,
  onValueSelected,
  onSelectedId,
  onPopOverSelected,
}: IStates) => {
  const handleDropdownSelected = () => {
    onValueSelected(item.name);
    onSelectedId(item.id);
    onPopOverSelected(false);
  };

  return (
    <li
      className={clsx({
        'dropdown-checked': item.name === valueSelect,
        'dropdown-user': mode === DROPDOWN_MODE.USER,
        'dropdown-color': mode === DROPDOWN_MODE.COLOR,
        'dropdown-color-checked': mode === DROPDOWN_MODE.COLOR,
        'dropdown-status': mode === DROPDOWN_MODE.STATUS,
      })}
      onClick={handleDropdownSelected}
    >
      <span
        className={clsx({
          'dropdown-user-item': mode === DROPDOWN_MODE.USER,
          'dropdown-color-item': mode === DROPDOWN_MODE.COLOR,
          'dropdown-color-item-success': item.name === COLOR.SUCCESS,
          'dropdown-color-item-warning': item.name === COLOR.WARNING,
          'dropdown-color-item-danger': item.name === COLOR.DANGER,
          'dropdown-status-item': mode === DROPDOWN_MODE.STATUS,
        })}
      />
        {item.name}
    </li>
  );
};

export default memo(DropdownItem);
