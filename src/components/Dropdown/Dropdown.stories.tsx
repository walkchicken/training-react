import { ComponentStory, ComponentMeta } from '@storybook/react';
import './dropdown.scss';
import 'containers/Form/Edit/formEdit.scss';

import Dropdown from '.';
import { DROPDOWN_MODE } from 'constants/mode';

export default {
  title: 'Dropdown',
  component: Dropdown,
} as ComponentMeta<typeof Dropdown>;

const Template: ComponentStory<typeof Dropdown> = (args) => (
  <Dropdown {...args} />
);

const DropdownUser = Template.bind({});
DropdownUser.args = {
  mode: DROPDOWN_MODE.USER,
  dataDropdown: [
    {
      id: 1,
      name: 'Jayce Crist',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/851.jpg',
      isChecked: true,
    },
    {
      id: 2,
      name: 'Dahlia Gulgowski',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/922.jpg',
      isChecked: true,
    },
    {
      id: 3,
      name: 'Lacey Feeneys',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1192.jpg',
      isChecked: true,
    },
    {
      id: 4,
      name: 'Sasha Kovacek',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1227.jpg',
      isChecked: false,
    },
    {
      id: 5,
      name: 'Lou Collins',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/235.jpg',
      isChecked: false,
    },
    {
      id: 6,
      name: 'Merl McClure',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/422.jpg',
      isChecked: true,
    },
    {
      id: 7,
      name: 'Roosevelt Trantow',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/228.jpg',
      isChecked: false,
    },
    {
      id: 8,
      name: 'Elyse Schuppe',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/231.jpg',
      isChecked: false,
    },
    {
      id: 9,
      name: 'Stone Brakus',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1006.jpg',
      isChecked: true,
    },
    {
      id: 10,
      name: 'Lelah Dickinson',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/134.jpg',
      isChecked: true,
    },
  ],
  valueSelect: 'Jayce Crist',
  popOverDropdown: false,
};

const DropdownColor = Template.bind({});
DropdownColor.args = {
  mode: DROPDOWN_MODE.COLOR,
  dataDropdown: [
    {
      id: 1,
      name: 'Normal',
      isChecked: false,
    },
    {
      id: 2,
      name: 'Low',
      isChecked: true,
    },
    {
      id: 3,
      name: 'Urgent',
      isChecked: true,
    },
  ],
  valueSelect: 'Urgent',
  popOverDropdown: false,
};

const DropdownStatus = Template.bind({});
DropdownStatus.args = {
  mode: DROPDOWN_MODE.STATUS,
  dataDropdown: [
    {
      id: 1,
      name: 'New',
      isChecked: false,
    },
    {
      id: 2,
      name: 'Work',
      isChecked: true,
    },
    {
      id: 3,
      name: 'Ready',
      isChecked: true,
    },
    {
      id: 4,
      name: 'Test',
      isChecked: true,
    },
    {
      id: 5,
      name: 'Done',
      isChecked: true,
    },
  ],
  valueSelect: 'New',
  popOverDropdown: false,
};

export { DropdownUser, DropdownColor, DropdownStatus };
