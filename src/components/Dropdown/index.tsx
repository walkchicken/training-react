// Style.
import './dropdown.scss';

// Lib.
import clsx from 'clsx';

// Constant.
import { DROPDOWN_MODE } from 'constants/mode';
import { COLOR } from 'constants/actions/color';

// Interface.
import { IUser } from 'types/user';
import { ICheckedInfo } from 'types/common';

// Component.
import DropdownItem from 'components/Dropdown/DropdownItem';

interface IProps {
  mode: string;
  dataDropdown: IUser[] | ICheckedInfo[];
  valueSelect?: string;
  onValueSelected: (valueSelected: string) => void;
  onSelectedId: (selectedId: number) => void;
  onPopOverSelected: (popOverSelected: boolean) => void;
  popOverDropdown: boolean;
  handlePopOverDropdown: () => void;
}

const Dropdown = ({
  mode,
  dataDropdown,
  valueSelect,
  onValueSelected,
  onSelectedId,
  onPopOverSelected,
  popOverDropdown,
  handlePopOverDropdown,
}: IProps) => {
  /**
   * show data users with JSX element
   * @returns JSX element li
   */
  const renderItemDropdown = () => {
    return dataDropdown.map((item, index) => (
      <DropdownItem
        key={`${item.id}_${index}`}
        mode={mode}
        item={item}
        valueSelect={valueSelect}
        onValueSelected={onValueSelected}
        onSelectedId={onSelectedId}
        onPopOverSelected={onPopOverSelected}
      />
    ));
  };

  return (
    <>
      <div className='combobox-input' onClick={handlePopOverDropdown}>
        {mode === DROPDOWN_MODE.COLOR && (
          <span
            className={clsx('dropdown-color-item', {
              'dropdown-color-item-success': valueSelect === COLOR.SUCCESS,
              'dropdown-color-item-warning': valueSelect === COLOR.WARNING,
              'dropdown-color-item-danger': valueSelect === COLOR.DANGER,
            })}
          />
        )}
        <input
          className={clsx('input-dropdown', {
            'input-dropdown-user': mode === DROPDOWN_MODE.USER,
          })}
          type='input'
          value={valueSelect}
          readOnly={true}
        />
        <span className='icon-menu-down mdi mdi-menu-down'></span>
      </div>
      {popOverDropdown && (
        <div
          className={clsx('dropdown', {
            'dropdown-color-mode': mode === DROPDOWN_MODE.COLOR,
          })}
        >
          <div
            className={clsx({
              'dropdown-contents': mode === DROPDOWN_MODE.USER,
            })}
          >
            <ul className='dropdown-scroll'>{renderItemDropdown()}</ul>
          </div>
        </div>
      )}
    </>
  );
};

export default Dropdown;
