// Style.
import './inProgressAndDoneList.scss';

// Lib.
import { memo } from 'react';

// Constant.
import { iconStyles, listStyles, titleStyles } from 'constants/className';

// Interface.
import { IList } from 'types/list';

interface IProps {
  columnId: string;
  column: IList;
  columnName: string;
  columnContent: JSX.Element;
}

const InProgressAndDoneList = ({
  columnId,
  column,
  columnName,
  columnContent,
}: IProps) => {
  return (
    <div key={columnId} className={listStyles[column.name]}>
      <div className={titleStyles[columnName]}>
        <span className='list-name'>{columnName}</span>
        <span className={iconStyles[columnName]} />
      </div>
      {columnContent}
    </div>
  );
};

export default memo(InProgressAndDoneList);
