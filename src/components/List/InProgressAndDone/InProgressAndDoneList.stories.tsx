import { ComponentStory, ComponentMeta } from '@storybook/react';
import InProgressAndDoneList from './index';
import './inProgressAndDoneList.scss';
import { LABEL } from 'constants/actions/list';

export default {
  title: 'InProgressAndDoneList',
  component: InProgressAndDoneList,
} as ComponentMeta<typeof InProgressAndDoneList>;

const Template: ComponentStory<typeof InProgressAndDoneList> = (args) => (
  <InProgressAndDoneList {...args} />
);

const InProgressAndDoneListTitle = Template.bind({});
InProgressAndDoneListTitle.args = {
  columnId: '2',
  column: {
    id: 2,
    name: 'InProgress',
    taskId: [1, 2],
  },
  columnName: LABEL.IN_PROGRESS,
};

export { InProgressAndDoneListTitle };
