import { ComponentStory, ComponentMeta } from '@storybook/react';
import BacklogList from './index';
import './backlogList.scss';
import { LABEL } from 'constants/actions/list';

export default {
  title: 'BacklogList',
  component: BacklogList,
} as ComponentMeta<typeof BacklogList>;

const Template: ComponentStory<typeof BacklogList> = (args) => (
  <BacklogList {...args} />
);

const BacklogListTitle = Template.bind({});
BacklogListTitle.args = {
  columnId: '1',
  column: {
    id: 1,
    name: 'Backlog',
    taskId: [1, 2],
  },
  columnName: LABEL.BACKLOG,
};

export { BacklogListTitle };
