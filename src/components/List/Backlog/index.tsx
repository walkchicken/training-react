// Style.
import './backlogList.scss';

// Lib.
import { memo } from 'react';

// Constant.
import { iconStyles, listStyles, titleStyles } from 'constants/className';

// Interface.
import { IList } from 'types/list';

interface IProps {
  columnId: string;
  column: IList;
  columnName: string;
  columnContent: JSX.Element;
  handleAddTask: (listBacklog: IList) => void;
}

const BacklogList = ({
  columnId,
  column,
  columnName,
  columnContent,
  handleAddTask,
}: IProps) => {
  return (
    <div key={columnId} className={listStyles[column.name]}>
      <div className={titleStyles[columnName]}>
        <span
          className={iconStyles[columnName]}
          onClick={() => handleAddTask(column)}
        />
        <span>{columnName}</span>
      </div>

      {columnContent}
    </div>
  );
};

export default memo(BacklogList);
