// Style.
import './testList.scss';

// Interface.
import { IList } from 'types/list';

interface IProps {
  columnId: string;
  column: IList;
  columnName: string;
  columnContent: JSX.Element;
}

const TestList = ({ columnId, column, columnName, columnContent }: IProps) => {
  return (
    <div key={columnId} className='list list-test'>
      {column.id !== 4 && (
        <div className='list-title'>
          <span className='list-name list-test-name'>Testing</span>
          <span className='icon-chevron list-test-icon mdi mdi-chevron-left'></span>
        </div>
      )}
      <div className='list-test-wrapper'>
        <div className='list-test-header'>{columnName}</div>
        {columnContent}
      </div>
    </div>
  );
};

export default TestList;
