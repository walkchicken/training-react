import { ComponentStory, ComponentMeta } from '@storybook/react';
import TestList from './index';
import './testList.scss';
import { LABEL } from 'constants/actions/list';

export default {
  title: 'TestList',
  component: TestList,
} as ComponentMeta<typeof TestList>;

const Template: ComponentStory<typeof TestList> = (args) => (
  <TestList {...args} />
);

const TestListTitle = Template.bind({});
TestListTitle.args = {
  columnId: '3',
  column: {
    id: 3,
    name: 'Ready to test',
    taskId: [4, 5],
  },
  columnName: LABEL.READY_TEST,
};

export { TestListTitle };
