import { ComponentStory, ComponentMeta } from '@storybook/react';
import TextArea from './index';
import './textarea.scss';

export default {
  title: 'TextArea',
  component: TextArea,
} as ComponentMeta<typeof TextArea>;

const Template: ComponentStory<typeof TextArea> = (args) => (
  <TextArea {...args} />
);

const TextAreaTitle = Template.bind({});
TextAreaTitle.args = {
  valueTextArea: 'Example',
};

export { TextArea };
