// Style.
import './textarea.scss';

// Lib.
import { ChangeEvent } from 'react';

interface IProps {
  valueTextArea: string;
  onSetValueTextArea: (valueTextArea: string) => void;
  refTextArea?: React.MutableRefObject<HTMLTextAreaElement>;
  onFocusTextArea?: (focusTextArea: boolean) => void;
}

const TextArea = ({
  valueTextArea,
  onSetValueTextArea,
  refTextArea,
  onFocusTextArea,
}: IProps) => {
  const handleSetValueTextArea = (event: ChangeEvent<HTMLTextAreaElement>) => {
    onSetValueTextArea(event.target.value);
  };

  const handleFocusTextArea = () => {
    if (onFocusTextArea) {
      onFocusTextArea(true);
    }
  };

  return (
    <textarea
      ref={refTextArea}
      className='textarea'
      placeholder='Type here..'
      name='texts'
      value={valueTextArea}
      id='texts'
      onChange={(event) => handleSetValueTextArea(event)}
      onFocus={handleFocusTextArea}
    />
  );
};

export default TextArea;
