// Clsx.
import clsx from 'clsx';

// Lib.
import { memo } from 'react';

// Interface.
import { IUser } from 'types/user';

interface IStates {
  user: IUser;
  onUserFilter: (userFilter: string) => void;
  userSelected: number;
  onUserSelected: (userSelected: number) => void;
  onToggleFilterUser: (toggleFilterUser: boolean) => void;
}

/**
 * @param user is data from renderUser and type of IUser
 * @param setUserFilter is handle selected name user filer
 * @returns JSX element li with data user
 */
const User = ({
  user,
  onUserFilter,
  userSelected,
  onUserSelected,
  onToggleFilterUser,
}: IStates) => {
  const handleFilterUser = () => {
    onUserFilter(user.name);
    onUserSelected(user.id);
    onToggleFilterUser(false);
  };

  return (
    <li
      className={clsx('list-item list-item-user', {
        'list-item-user-checked': userSelected === user.id,
      })}
      onClick={handleFilterUser}
    >
      <span className='user-filter'>
        <span className='user-filter-image'>
          <img className='avatar-user' src={user.avatar} alt='avatar-user' />
        </span>
        <span className='user-filter-name'>{user.name}</span>
      </span>
    </li>
  );
};

export default memo(User);
