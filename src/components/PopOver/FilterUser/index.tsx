// Style.
import './filterUser.scss';

// Lib.
import { memo } from 'react';

// Component.
import User from './User';
import { IUser } from 'types/user';

interface IStates {
  dataUsers: IUser[];
  onUserFilter: (userFilter: string) => void;
  userSelected: number;
  onUserSelected: (userSelected: number) => void;
  onToggleFilterUser: (toggleFilterUser: boolean) => void;
}

const PopOverFilterUser = ({
  dataUsers,
  onUserFilter,
  userSelected,
  onUserSelected,
  onToggleFilterUser,
}: IStates) => {
  /**
   * show data users with JSX element
   * @returns JSX element li
   */
  const renderUsers = () => {
    return dataUsers.map((user, index) => (
      <User
        key={`${user.id}_${index}`}
        user={user}
        onUserFilter={onUserFilter}
        userSelected={userSelected}
        onUserSelected={onUserSelected}
        onToggleFilterUser={onToggleFilterUser}
      />
    ));
  };

  // Set default filter user.
  const handleFilterUser = () => {
    onUserFilter('Users');
    onUserSelected(0);
    onToggleFilterUser(false);
  };

  return (
    <div className='popup-filter'>
      <div className='popup-filter-contents'>
        <ul className='popup-filter-scroll'>
          <li className='list-item' onClick={handleFilterUser}>
            <span className='reset-filter'>
              <span className='mdi mdi-delete-forever icon-delete-forever'></span>
              <span className='reset-filter-text'>Reset Filters</span>
            </span>
          </li>
          {renderUsers()}
        </ul>
      </div>
    </div>
  );
};

export default memo(PopOverFilterUser);
