import { ComponentStory, ComponentMeta } from '@storybook/react';
import PopOverFilterUser from './index';
import './filterUser.scss';

export default {
  title: 'PopOverFilterUser',
  component: PopOverFilterUser,
} as ComponentMeta<typeof PopOverFilterUser>;

const Template: ComponentStory<typeof PopOverFilterUser> = (args) => (
  <PopOverFilterUser {...args} />
);

const FilterUser = Template.bind({});
FilterUser.args = {
  dataUsers: [
    {
      id: 1,
      name: 'Jayce Crist',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/851.jpg',
      isChecked: true,
    },
    {
      id: 2,
      name: 'Dahlia Gulgowski',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/922.jpg',
      isChecked: true,
    },
    {
      id: 3,
      name: 'Lacey Feeneys',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1192.jpg',
      isChecked: true,
    },
    {
      id: 4,
      name: 'Sasha Kovacek',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1227.jpg',
      isChecked: false,
    },
    {
      id: 5,
      name: 'Lou Collins',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/235.jpg',
      isChecked: false,
    },
    {
      id: 6,
      name: 'Merl McClure',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/422.jpg',
      isChecked: true,
    },
    {
      id: 7,
      name: 'Roosevelt Trantow',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/228.jpg',
      isChecked: false,
    },
    {
      id: 8,
      name: 'Elyse Schuppe',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/231.jpg',
      isChecked: false,
    },
    {
      id: 9,
      name: 'Stone Brakus',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1006.jpg',
      isChecked: true,
    },
    {
      id: 10,
      name: 'Lelah Dickinson',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/134.jpg',
      isChecked: true,
    },
  ],
  userSelected: 2,
};

export { FilterUser };
