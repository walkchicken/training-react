// Clsx.
import { updateTask } from 'actions/task';
import clsx from 'clsx';
import { FETCH_TASK } from 'constants/actions/task';

// Lib.
import { memo } from 'react';
import { IDispatch } from 'types/common';
import { ITask } from 'types/task';

// Interface.
import { IUser } from 'types/user';

interface IStates {
  user: IUser;
  dispatchTasks: IDispatch<ITask>;
  task: ITask;
  userInTask?: IUser;
  onUserInTask: (userInTask: IUser) => void;
  onPopupAvatarUser: (popUpAvatarUser: boolean) => void;
}

/**
 * @param user is data from renderUser and type of IUser
 * @param userInTask is user selected.
 * @returns JSX element li with data user
 */
const AvatarUser = ({
  user,
  task,
  dispatchTasks,
  userInTask,
  onUserInTask,
  onPopupAvatarUser,
}: IStates) => {
  const handleSetUserInTask = () => {
    onUserInTask(user);

    const valueEditTask = {
      id: task.id,
      title: task.title,
      tagId: task.tagId,
      userId: user.id,
      colorId: task.colorId,
      statusId: task.statusId,
      fileId: [2, 1, 3],
      commentId: [2],
    };

    // Call to action update task.
    updateTask(
      dispatchTasks,
      FETCH_TASK.UPDATING,
      FETCH_TASK.UPDATE_SUCCESS,
      FETCH_TASK.UPDATE_ERROR,
      valueEditTask,
    );

    onPopupAvatarUser(false);
  };

  return (
    <li
      className={clsx('list-avatar', {
        'list-avatar-checked': userInTask && user.id === userInTask.id,
      })}
      onClick={handleSetUserInTask}
    >
      <span className='avatar-select-image'>
        <img className='avatar' src={user.avatar} alt='avatar-user' />
      </span>
      <span className='avatar-select-name'>{user.name}</span>
    </li>
  );
};

export default memo(AvatarUser);
