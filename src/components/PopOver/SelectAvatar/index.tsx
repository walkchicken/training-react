// Style.
import './popOverSelectAvatar.scss';

// Lib.
import { memo } from 'react';

// Interface.
import { ITask } from 'types/task';
import { IDispatch } from 'types/common';
import { IUser } from 'types/user';

// Component.
import AvatarUser from './AvatarUser';

interface IStates {
  dataUsers: IUser[];
  dispatchTasks: IDispatch<ITask>;
  task: ITask;
  userInTask?: IUser;
  onUserInTask: (userInTask: IUser) => void;
  onPopupAvatarUser: (popUpAvatarUser: boolean) => void;
}

const PopOverSelectAvatar = ({
  dataUsers,
  dispatchTasks,
  task,
  userInTask,
  onUserInTask,
  onPopupAvatarUser,
}: IStates) => {
  /**
   * render avatar user.
   * @returns JSX element li.
   */
  const renderUsers = () => {
    return dataUsers.map((user, index) => (
      <AvatarUser
        key={`${user.id}_${index}`}
        user={user}
        dispatchTasks={dispatchTasks}
        task={task}
        userInTask={userInTask}
        onUserInTask={onUserInTask}
        onPopupAvatarUser={onPopupAvatarUser}
      />
    ));
  };

  return (
    <div className='popover-select-avatar'>
      <div className='popover-select-avatar-contents'>
        <ul className='popover-select-avatar-scroll'>{renderUsers()}</ul>
      </div>
    </div>
  );
};

export default memo(PopOverSelectAvatar);
