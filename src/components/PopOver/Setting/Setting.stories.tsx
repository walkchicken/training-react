import { ComponentStory, ComponentMeta } from '@storybook/react';
import PopOverSetting from './index';
import './popOverSetting.scss';

export default {
  title: 'PopOverSetting',
  component: PopOverSetting,
} as ComponentMeta<typeof PopOverSetting>;

const Template: ComponentStory<typeof PopOverSetting> = (args) => (
  <PopOverSetting {...args} />
);

const Setting = Template.bind({});
Setting.args = {};

export { Setting };
