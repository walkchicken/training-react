// Style.
import './popOverSetting.scss';

// Lib.
import { memo } from 'react';

// Constant.
import { FETCH_LIST } from 'constants/actions/list';
import { FETCH_TASK } from 'constants/actions/task';

// Action.
import {
  updateListAfterAddTask,
  updateListAfterRemoveTask,
} from 'actions/list';
import { addNewTask, deleteTask } from 'actions/task';

// Interface.
import { ICheckedInfo, IDispatch, IResponse } from 'types/common';
import { IList } from 'types/list';
import { IEditTask, IFile, ITask } from 'types/task';
import { IUser } from 'types/user';

interface IProps {
  task: ITask;
  column: IList;
  userInTask: IUser;
  colorTask: ICheckedInfo | undefined;
  statusTask: ICheckedInfo | undefined;
  fileInTask: IFile[] | undefined;
  dispatchLists: IDispatch<IList>;
  dataLists: IResponse<IList>;
  dispatchTasks: IDispatch<ITask>;
  dataTasks: IResponse<ITask>;
  handleEdit: (isShow: boolean, task?: IEditTask) => void;
  setTogglePopOverSetting: (togglePopOverSetting: boolean) => void;
  dataUsers: IResponse<IUser>;
  dataColors: IResponse<ICheckedInfo>;
  dataStatuses: IResponse<ICheckedInfo>;
}

const PopOverSetting = ({
  task,
  column,
  userInTask,
  colorTask,
  statusTask,
  fileInTask,
  dispatchLists,
  dataLists,
  dispatchTasks,
  dataTasks,
  handleEdit,
  setTogglePopOverSetting,
  dataUsers,
  dataColors,
  dataStatuses,
}: IProps) => {
  const handleSettingEditTask = () => {
    handleEdit(!false, {
      ...task,
      userInTask,
      colorTask,
      statusTask,
      fileInTask,
      column,
      dataLists,
      dataTasks,
      dispatchLists,
      dispatchTasks,
      dataUsers,
      dataColors,
      dataStatuses,
    });
    setTogglePopOverSetting(false);
  };

  const handleSettingRemoveTask = () => {
    updateListAfterRemoveTask(
      dispatchLists,
      FETCH_LIST.REMOVING,
      FETCH_LIST.REMOVE_SUCCESS,
      FETCH_LIST.REMOVE_ERROR,
      dataLists.data,
      column,
      task.id,
    );

    deleteTask(
      dispatchTasks,
      FETCH_TASK.REMOVING,
      FETCH_TASK.REMOVE_SUCCESS,
      FETCH_TASK.REMOVE_ERROR,
      dataTasks.data,
      task.id,
    );

    setTogglePopOverSetting(false);
  };

  const handleSettingCopyTask = () => {
    const valueTaskCopy = {
      id: dataTasks.data.length + 1,
      title: task.title,
      tagId: task.tagId,
      userId: task.userId,
      colorId: task.colorId,
      statusId: task.statusId,
      fileId: task.fileId,
      commentId: task.commentId,
    };

    addNewTask(
      dispatchTasks,
      FETCH_TASK.ADDING,
      FETCH_TASK.ADD_SUCCESS,
      FETCH_TASK.ADD_ERROR,
      valueTaskCopy,
    );

    const fetchUpdateListWithTaskCopy = async () => {
      return await updateListAfterAddTask(
        dispatchLists,
        FETCH_LIST.ADDING,
        FETCH_LIST.ADD_SUCCESS,
        FETCH_LIST.ADD_ERROR,
        column,
        valueTaskCopy,
      );
    };

    fetchUpdateListWithTaskCopy();
    setTogglePopOverSetting(false);
  };

  return (
    <div className='popover-setting'>
      <div
        className='setting-item setting-item-edit'
        onClick={handleSettingEditTask}
      >
        Edit
      </div>
      <div
        className='setting-item setting-item-copy'
        onClick={handleSettingCopyTask}
      >
        Copy
      </div>
      <div
        className='setting-item setting-item-remove'
        onClick={handleSettingRemoveTask}
      >
        Remove
      </div>
    </div>
  );
};

export default memo(PopOverSetting);
