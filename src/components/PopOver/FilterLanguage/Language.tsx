import clsx from 'clsx';
import { ICheckedInfo } from 'types/common';

interface IProps {
  language: ICheckedInfo;
  onFilterLanguage: (language: string) => void;
  languageSelected: number;
  onLanguageSelected: (languageSelected: number) => void;
  onToggleFilterLanguage: (toggleFilterLanguage: boolean) => void;
}

export const Language = ({
  language,
  onFilterLanguage,
  languageSelected,
  onLanguageSelected,
  onToggleFilterLanguage,
}: IProps) => {
  const handleFilterLanguage = () => {
    onFilterLanguage(language.name);
    onLanguageSelected(language.id);
    onToggleFilterLanguage(false);
  };

  return (
    <li
      className={clsx('filter-language-item', {
        'item-selected': languageSelected === language.id,
      })}
      onClick={handleFilterLanguage}
    >
      {language.name}
    </li>
  );
};
