// Style.
import './filterLanguage.scss';

// Lib.
import { memo } from 'react';

// Component.
import { Language } from 'components/PopOver/FilterLanguage/Language';

const languages = [
  {
    id: 1,
    name: 'English',
    isChecked: true,
  },
  {
    id: 2,
    name: 'Русский',
    isChecked: false,
  },
  {
    id: 3,
    name: 'Deutsch',
    isChecked: false,
  },
];

interface IStates {
  onFilterLanguage: (language: string) => void;
  languageSelected: number;
  onLanguageSelected: (languageSelected: number) => void;
  onToggleFilterLanguage: (toggleFilterLanguage: boolean) => void;
}

const PopOverFilterLanguage = ({
  onFilterLanguage,
  languageSelected,
  onLanguageSelected,
  onToggleFilterLanguage,
}: IStates) => {
  /**
   * Render data language from languages array
   * @returns JSX element li
   */
  const renderLanguages = () => {
    return (
      // Map array languages
      languages.map((language) => (
        <Language
          key={language.id}
          language={language}
          onFilterLanguage={onFilterLanguage}
          languageSelected={languageSelected}
          onLanguageSelected={onLanguageSelected}
          onToggleFilterLanguage={onToggleFilterLanguage}
        />
      ))
    );
  };

  return (
    <div className='filter-language'>
      <div className='filter-language-content'>
        <ul className='filter-language-items'>{renderLanguages()}</ul>
      </div>
    </div>
  );
};

export default memo(PopOverFilterLanguage);
