import { ComponentStory, ComponentMeta } from '@storybook/react';
import PopOverFilterLanguage from './index';
import './filterLanguage.scss';

export default {
  title: 'PopOverFilterLanguage',
  component: PopOverFilterLanguage,
} as ComponentMeta<typeof PopOverFilterLanguage>;

const Template: ComponentStory<typeof PopOverFilterLanguage> = (args) => (
  <PopOverFilterLanguage {...args} />
);

const FilterLanguage = Template.bind({});
FilterLanguage.args = {
  languageSelected: 1,
};

export { FilterLanguage };
