// Style.
import './popOverComment.scss';

// Interface.
import { IComment } from 'types/task';
import { IUser } from 'types/user';

interface IProps {
  comment: IComment;
  dataUsers: IUser[];
}

const Comment = ({ comment, dataUsers }: IProps) => {
  const userInComment = dataUsers.find((item) => item.id === comment.userId);

  return (
    <div className='comment'>
      <div className='avatar-user-comment'>
        {userInComment ? (
          <img
            className='avatar-user-comment-img'
            src={userInComment.avatar}
            alt='avatar comment'
          />
        ) : (
          <span className='icon-task icon-account mdi mdi-account'></span>
        )}
      </div>
      <div className='comment-content'>
        <div className='comment-content-top'>
          <span className='name-user-comment'>{userInComment?.name}</span>
          <span className='date-comment'>{comment.time}</span>
        </div>
        <div className='comment-content-bottom'>{comment.comment}</div>
      </div>
    </div>
  );
};

export default Comment;
