// Style.
import './popOverComment.scss';

// Lib.
import { memo, useState, useRef } from 'react';
import clsx from 'clsx';

// Constant.
import { FETCH_COMMENT } from 'constants/actions/comment';
import { FETCH_TASK } from 'constants/actions/task';

// Interface.
import { IComment, ITask } from 'types/task';
import { IDispatch } from 'types/common';
import { IUser } from 'types/user';

// Action.
import { addNewComment } from 'actions/comment';
import { updateTaskAfterAddComment } from 'actions/task';

// Component.
import Button from 'components/Button';
import TextArea from 'components/TextArea';
import Comment from 'components/PopOver/Comment/Comment';

interface IProps {
  task: ITask;
  dataComments: IComment[];
  dispatchComments: IDispatch<IComment>;
  dataUsers: IUser[];
  dispatchTasks: IDispatch<ITask>;
  commentInTask: IComment[];
}

const PopOverComment = ({
  task,
  dataComments,
  dispatchComments,
  dataUsers,
  dispatchTasks,
  commentInTask,
}: IProps) => {
  const [valueComment, setValueComment] = useState('');
  const [focusComment, setFocusComment] = useState(false);
  const refTextArea = useRef<HTMLTextAreaElement>({} as HTMLTextAreaElement);

  const renderComment = () => {
    return commentInTask.map((comment, index) => (
      <Comment key={comment.id} comment={comment} dataUsers={dataUsers} />
    ));
  };

  const handleSendComment = () => {
    const timeComment = new Date();

    const valueNewComment = {
      id: dataComments.length + 1,
      comment: valueComment,
      time:
        timeComment.getFullYear() +
        '-' +
        (timeComment.getMonth() + 1) +
        '-' +
        timeComment.getDate(),
      userId: 0,
    };

    addNewComment(
      dispatchComments,
      FETCH_COMMENT.ADDING,
      FETCH_COMMENT.ADD_SUCCESS,
      FETCH_COMMENT.ADD_ERROR,
      valueNewComment,
    );

    const fetchUpdateDataCommentTask = async () => {
      return await updateTaskAfterAddComment(
        dispatchTasks,
        FETCH_TASK.UPDATING_COMMENT,
        FETCH_TASK.UPDATE_COMMENT_SUCCESS,
        FETCH_TASK.UPDATE_COMMENT_ERROR,
        task,
        valueNewComment,
      );
    };

    fetchUpdateDataCommentTask();
    setValueComment('');
  };

  const handleFocusComment = () => {
    setFocusComment(false);
    refTextArea.current.blur();
  };

  return (
    <div className='popover-comment' onClick={handleFocusComment}>
      <div
        className={clsx('popover-comment-content', {
          'popover-comment-content-focus': focusComment,
        })}
      >
        {renderComment()}
      </div>
      <div
        className={clsx('add-comment', { 'add-comment-focus': focusComment })}
      >
        <div
          className={clsx('type-comment', {
            'type-comment-focus': focusComment,
          })}
          onClick={(event) => event.stopPropagation()}
        >
          <TextArea
            valueTextArea={valueComment}
            onSetValueTextArea={setValueComment}
            refTextArea={refTextArea}
            onFocusTextArea={setFocusComment}
          />
        </div>
        {focusComment && (
          <div className='add-comment-button'>
            <div className='button-send'>
              <Button primary={false} icon={false} onClick={handleSendComment}>
                Send
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default memo(PopOverComment);
