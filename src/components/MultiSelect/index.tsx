// Lib.
import clsx from 'clsx';

// Action.
import { renderTagById } from 'actions/renderTagById';

// Interface.
import { ICheckedInfo } from 'types/common';
import { IEditTask } from 'types/task';

// Component.
import { useRef } from 'react';
import ModalTag from './ModalTag';
import './multiSelect.scss';

interface IProps {
  selectedTagId: number[];
  valuePopupEdit: {
    popupFormEdit: boolean;
    handleEdit: (popupFormEdit: boolean, value?: IEditTask) => void;
  };
  onClickDeleteTagId: (valueDelete: number) => void;
  tags: ICheckedInfo[];
  valueTagId: number[];
  onSetValueTagId: (valueTagId: number[]) => void;
  isSelectedTagId: (value: number) => boolean;
  onChangeSelectedTagId: (event: React.ChangeEvent<HTMLInputElement>) => void;
  popOverTag: boolean;
  setPopOverTag: (popOverTag: boolean) => void;
  onPopOverAssignUser: (popOverAssignUser: boolean) => void;
  onPopOverSelectColor: (popOverSelectColor: boolean) => void;
  onPopOverSelectStatus: (popOverSelectStatus: boolean) => void;
}

const MultiSelect = ({
  selectedTagId,
  valuePopupEdit,
  onClickDeleteTagId,
  tags,
  valueTagId,
  onSetValueTagId,
  isSelectedTagId,
  onChangeSelectedTagId,
  onPopOverAssignUser,
  onPopOverSelectColor,
  onPopOverSelectStatus,
  popOverTag,
  setPopOverTag,
}: IProps) => {
  const inputTag = useRef<HTMLInputElement>({} as HTMLInputElement);

  const renderLabelTag = (item: number, index: number) => {
    return (
      <label
        key={`${item}_${index}`}
        className={clsx('tag', {
          'tag-checked': valuePopupEdit.popupFormEdit,
        })}
      >
        {renderTagById(item)}
        {valuePopupEdit.popupFormEdit && (
          <span
            onClick={() => onClickDeleteTagId(item)}
            className='mdi mdi-close-circle icon-close'
          />
        )}
      </label>
    );
  };

  // Render model select tag.
  const renderModalTag = () => {
    return tags.map((tag, index) => (
      <ModalTag
        key={`${tag.id}_${index}`}
        tag={tag}
        valueTagId={valueTagId}
        onSetValueTagId={onSetValueTagId}
        isSelectedTagId={isSelectedTagId}
        onChangeSelectedTagId={onChangeSelectedTagId}
      />
    ));
  };

  // Handle open | close popup form model tag.
  const handleFocus = () => {
    setPopOverTag(!popOverTag);
    onPopOverSelectStatus(false);
    onPopOverSelectColor(false);
    onPopOverAssignUser(false);
    inputTag.current.blur();
  };

  return (
    <div className='form-edit-input-tag'>
      {/* Render tag */}
      <ul className='tag-item'>
        {selectedTagId.map((item, index) => renderLabelTag(item, index))}
      </ul>

      {/* Popup select tag modal. */}
      <input ref={inputTag} className='select-focus' onFocus={handleFocus} />
      {popOverTag && (
        <div className='modal-tag'>
          <div className='modal-tag-wrapper'>{renderModalTag()}</div>
        </div>
      )}
    </div>
  );
};

export default MultiSelect;
