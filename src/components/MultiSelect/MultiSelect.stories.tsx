import { ComponentStory, ComponentMeta } from '@storybook/react';
import { IEditTask } from 'types/task';
import MultiSelect from '.';
import './multiSelect.scss';
import 'containers/Form/Edit/formEdit.scss';

export default {
  title: 'MultiSelect',
  component: MultiSelect,
} as ComponentMeta<typeof MultiSelect>;

const Template: ComponentStory<typeof MultiSelect> = (args) => (
  <MultiSelect {...args} />
);

const MultiSelectTag = Template.bind({});
MultiSelectTag.args = {
  selectedTagId: [2, 1, 3],
  valuePopupEdit: {
    popupFormEdit: true,
    handleEdit: (popupFormEdit: boolean, value?: IEditTask) => {
      alert('');
    },
  },
  isSelectedTagId: () => true,
  tags: [
    {
      id: 1,
      name: 'Webix',
      isChecked: true,
    },
    {
      id: 2,
      name: 'Jet',
      isChecked: true,
    },
    {
      id: 3,
      name: 'Easy',
      isChecked: true,
    },
    {
      id: 4,
      name: 'Hard',
      isChecked: true,
    },
    {
      id: 5,
      name: 'Kanban',
      isChecked: false,
    },
    {
      id: 6,
      name: 'Docs',
      isChecked: false,
    },
  ],
  valueTagId: [2, 1, 3],
  popOverTag: false,
};

export { MultiSelectTag };
