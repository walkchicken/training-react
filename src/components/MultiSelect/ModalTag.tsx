// Style.
import './multiSelect.scss';

// Interface.
import { ICheckedInfo } from 'types/common';

interface IProps {
  tag: ICheckedInfo;
  valueTagId: number[];
  onSetValueTagId: (setValueTagId: number[]) => void;
  isSelectedTagId: (value: number) => boolean;
  onChangeSelectedTagId: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const ModalTag = ({
  tag,
  valueTagId,
  onSetValueTagId,
  isSelectedTagId,
  onChangeSelectedTagId,
}: IProps) => {
  // Handle set selected tag id.
  const handleSetValueTagId = () => {
    // Splice tag id if checked.
    for (let i = 0; i < valueTagId.length; i++) {
      if (valueTagId[i] === tag.id) {
        valueTagId.splice(i, 1);
        onSetValueTagId(valueTagId);
      } else {
        onSetValueTagId([...valueTagId, tag.id]);
      }
    }
  };

  return (
    <div className='modal-tag-content'>
      <input
        id={`${tag.id}`}
        className='checkbox'
        type='checkbox'
        value={tag.id}
        checked={isSelectedTagId(tag.id)}
        onChange={onChangeSelectedTagId}
        onClick={handleSetValueTagId}
      />
      {tag.name}
    </div>
  );
};

export default ModalTag;
