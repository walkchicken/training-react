import { ComponentStory, ComponentMeta } from '@storybook/react';
import FilePreview from './index';
import './filePreview.scss';

export default {
  title: 'FilePreview',
  component: FilePreview,
} as ComponentMeta<typeof FilePreview>;

const Template: ComponentStory<typeof FilePreview> = (args) => (
  <FilePreview {...args} />
);

const File = Template.bind({});
File.args = {};

export { File };
