// Style.
import './filePreview.scss';

// Component.
import Button from 'components/Button';

interface IProps {
  file: File;
  onRemoveFileUpload: (nameFile: string) => void;
}

const FilePreview = ({ file, onRemoveFileUpload }: IProps) => {
  const handleRemoveFile = () => {
    onRemoveFileUpload(file.name);
  };

  const renderPreview = () => {
    try {
      return (
        <img
          className='image-preview'
          src={window.URL.createObjectURL(file)}
          alt='file'
        />
      );
    } catch (error) {
      return <p>{file as unknown as string}</p>;
    }
  };

  return (
    <div className='file-preview'>
      {renderPreview()}
      <div className='button-close-preview'>
        {/* Button close form edit. */}
        <Button primary={true} icon={true} onClick={handleRemoveFile}>
          <span className='icon-close-preview mdi mdi-close'></span>
        </Button>
      </div>
    </div>
  );
};

export default FilePreview;
