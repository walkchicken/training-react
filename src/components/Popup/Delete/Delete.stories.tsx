import { ComponentStory, ComponentMeta } from '@storybook/react';
import PopupDelete from './index';
import './popupDelete.scss';

export default {
  title: 'PopupDelete',
  component: PopupDelete,
} as ComponentMeta<typeof PopupDelete>;

const Template: ComponentStory<typeof PopupDelete> = (args) => (
  <PopupDelete {...args} />
);

const Delete = Template.bind({});
Delete.args = {};

export { Delete };
