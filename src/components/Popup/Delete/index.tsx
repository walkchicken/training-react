// Style.
import './popupDelete.scss';

// Lib.
import { memo } from 'react';

// Constant.
import { FETCH_LIST } from 'constants/actions/list';
import { FETCH_TASK } from 'constants/actions/task';

// Action.
import { updateListAfterRemoveTask } from 'actions/list';
import { deleteTask } from 'actions/task';

// Interface.
import { IEditTask } from 'types/task';

// Component.
import Button from 'components/Button';

interface IProps {
  onPopUpProcess: (popupProcess: boolean) => void;
  valuePopupEdit: {
    popupFormEdit: boolean;
    handleEdit: (popupFormEdit: boolean, value?: IEditTask) => void;
  };
  task: IEditTask;
}

const PopupDelete = ({ onPopUpProcess, valuePopupEdit, task }: IProps) => {
  const handlePopupProcess = () => {
    onPopUpProcess(false);
  };

  const handleRemoveTask = () => {
    if (task.column) {
      updateListAfterRemoveTask(
        task.dispatchLists,
        FETCH_LIST.REMOVING,
        FETCH_LIST.REMOVE_SUCCESS,
        FETCH_LIST.REMOVE_ERROR,
        task.dataLists.data,
        task.column,
        task.id,
      );

      deleteTask(
        task.dispatchTasks,
        FETCH_TASK.REMOVING,
        FETCH_TASK.REMOVE_SUCCESS,
        FETCH_TASK.REMOVE_ERROR,
        task.dataTasks.data,
        task.id,
      );
    }

    onPopUpProcess(false);
    valuePopupEdit.handleEdit(false);
  };

  return (
    <div className='popup-delete'>
      <div className='popup-delete-text'>
        <span>The card will be deleted permanently, are you sure?</span>
      </div>
      <div className='popup-delete-controls'>
        <div className='button-control button-cancel'>
          <Button primary={true} onClick={handlePopupProcess}>
            Cancel
          </Button>
        </div>
        <div className='button-control button-confirm'>
          <Button primary={false} onClick={handleRemoveTask}>
            OK
          </Button>
        </div>
      </div>
    </div>
  );
};
export default memo(PopupDelete);
