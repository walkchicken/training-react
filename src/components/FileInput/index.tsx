// Style
import './fileInput.scss';

interface IProps {
  inputUploadFileRef: React.MutableRefObject<HTMLInputElement>;
  onValueFile: (value: File) => void;
}

// Render input type file and get value input.
const FileInput = ({ inputUploadFileRef, onValueFile }: IProps) => {
  const handleUploadFile = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.files ? event.target.files[0] : null;

    if (value) {
      onValueFile(value);
    }
  };

  return (
    <input
      ref={inputUploadFileRef}
      className='upload-file'
      type='file'
      name='uploadFile'
      onChange={(event) => handleUploadFile(event)}
    />
  );
};

export default FileInput;
