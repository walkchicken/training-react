// Style.
import './count.scss';

// Interface.
import { IComment, IFile } from 'types/task';

interface IProps {
  countValue?: IComment[] | IFile[];
}

const Count = ({ countValue }: IProps) => {
  return (
    <span className='count'>
      {countValue && countValue.length > 0 && countValue.length}
    </span>
  );
};

export default Count;
