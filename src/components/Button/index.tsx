// Style.
import './button.scss';

// Lib.
import { memo, ReactNode } from 'react';

// Clsx.
import clsx from 'clsx';

interface IProps {
  primary?: boolean;
  children: ReactNode;
  onClick?: () => void;
  icon?: boolean;
  hover?: boolean;
}

/**
 * @param primary is handle color button
 * @param children is add text or contents in button
 * @param onClick handle event onClick button
 * @param icon is handle if icon
 * @returns button component
 */
const Button = ({
  primary = true,
  children,
  onClick,
  icon = false,
  hover = false,
}: IProps) => {
  const mode = primary ? 'btn-secondary' : 'btn-info';
  return (
    <button
      type='button'
      className={clsx([
        'btn',
        mode,
        { 'btn-icon': icon, 'btn-hover-info': hover },
      ])}
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default memo(Button);
