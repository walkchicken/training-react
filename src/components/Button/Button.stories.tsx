import { ComponentStory, ComponentMeta } from '@storybook/react';
import './button.scss';

import Button from './index';
import clsx from 'clsx';

export default {
  title: 'Button',
  component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

const ButtonPrimary = Template.bind({});
ButtonPrimary.args = {
  primary: true,
  children: (
    <>
      <span className={clsx('icon-export', ['mdi', 'mdi-export'])}></span>{' '}
      Export
    </>
  ),
  onClick: () => {
    alert('Export Button');
  },
};

const ButtonSecondary = Template.bind({});
ButtonSecondary.args = {
  primary: false,
  children: <span>OK</span>,
  onClick: () => {
    alert('Success');
  },
};

const ButtonIcon = Template.bind({});
ButtonIcon.args = {
  primary: true,
  children: <span className={clsx('icon-close', ['mdi', 'mdi-close'])}></span>,
  onClick: () => {
    alert('Close');
  },
  icon: true,
};

export { ButtonPrimary, ButtonSecondary, ButtonIcon };
