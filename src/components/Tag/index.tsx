// Style.
import './tag.scss';

// Clsx.
import clsx from 'clsx';

// Lib.
import { memo } from 'react';

// Interface.
import { ICheckedInfo } from 'types/common';

interface IProps {
  tag: ICheckedInfo;
  popupFormEdit?: boolean;
}

// Render tag.
const Tag = ({ tag, popupFormEdit }: IProps) => {
  return (
    <>
      <label className={clsx('tag', { 'tag-checked': popupFormEdit })}>
        {tag.name}
        {popupFormEdit && (
          <span className='mdi mdi-close-circle icon-close'></span>
        )}
      </label>
    </>
  );
};

/**
 * Component handle re-render tag.
 * @param prevTag all props prev tag.
 * @param nextTag all props next tag.
 * @returns if true then not render tag.
 */
const tagPropsAreEqual = (prevTag: IProps, nextTag: IProps) => {
  return prevTag.tag === nextTag.tag;
};
export default memo(Tag, tagPropsAreEqual);
