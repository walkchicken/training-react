import { ComponentStory, ComponentMeta } from '@storybook/react';
import './tag.scss';

import Tag from './index';

export default {
  title: 'Label',
  component: Tag,
} as ComponentMeta<typeof Tag>;

const Template: ComponentStory<typeof Tag> = (args) => <Tag {...args} />;

const TagDefault = Template.bind({});
TagDefault.args = {
  tag: {
    id: 1,
    name: 'Webix',
    isChecked: true,
  },
  popupFormEdit: false,
};

export { TagDefault };
