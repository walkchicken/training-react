import { ComponentStory, ComponentMeta } from '@storybook/react';
import ShowSelectAvatar from './SelectAvatar';
import 'containers/Task/task.scss';

export default {
  title: 'ShowSelectAvatar',
  component: ShowSelectAvatar,
} as ComponentMeta<typeof ShowSelectAvatar>;

const Template: ComponentStory<typeof ShowSelectAvatar> = (args) => (
  <ShowSelectAvatar {...args} />
);

const IconShowSelectAvatar = Template.bind({});
IconShowSelectAvatar.args = {
  popUpAvatarUser: true,
  dataUsers: [
    {
      id: 1,
      name: 'Jayce Crist',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/851.jpg',
      isChecked: true,
    },
    {
      id: 2,
      name: 'Dahlia Gulgowski',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/922.jpg',
      isChecked: true,
    },
    {
      id: 3,
      name: 'Lacey Feeneys',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1192.jpg',
      isChecked: true,
    },
    {
      id: 4,
      name: 'Sasha Kovacek',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1227.jpg',
      isChecked: false,
    },
    {
      id: 5,
      name: 'Lou Collins',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/235.jpg',
      isChecked: false,
    },
    {
      id: 6,
      name: 'Merl McClure',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/422.jpg',
      isChecked: true,
    },
    {
      id: 7,
      name: 'Roosevelt Trantow',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/228.jpg',
      isChecked: false,
    },
    {
      id: 8,
      name: 'Elyse Schuppe',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/231.jpg',
      isChecked: false,
    },
    {
      id: 9,
      name: 'Stone Brakus',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1006.jpg',
      isChecked: true,
    },
    {
      id: 10,
      name: 'Lelah Dickinson',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/134.jpg',
      isChecked: true,
    },
  ],
  task: {
    id: 3,
    title: 'Legacy Identity Producer',
    tagId: [6],
    userId: 3,
    colorId: 3,
    statusId: 3,
    fileId: [7],
    commentId: [1, 4],
  },
  userInTask: {
    id: 2,
    name: 'Dahlia Gulgowski',
    avatar:
      'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/922.jpg',
    isChecked: true,
  },
  selectedTask: 3,
};

export { IconShowSelectAvatar };
