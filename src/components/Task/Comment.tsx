// Lib.
import { Popover } from 'react-tiny-popover';

// Interface.
import { IUser } from 'types/user';
import { IDispatch } from 'types/common';
import { IComment, ITask } from 'types/task';

// Container.
import PopOverComment from 'components/PopOver/Comment';

// Component.
import Count from 'components/Count';

interface IProps {
  togglePopOverComment: boolean;
  dataComments: IComment[];
  dispatchComments: IDispatch<IComment>;
  dataUsers: IUser[];
  dispatchTasks: IDispatch<ITask>;
  task: ITask;
  commentInTask: IComment[];
  handlePopOverComment: () => void;
  selectedTask: number;
}

const ShowComment = ({
  togglePopOverComment,
  dataComments,
  dispatchComments,
  dataUsers,
  dispatchTasks,
  task,
  commentInTask,
  handlePopOverComment,
  selectedTask,
}: IProps) => {
  return (
    <Popover
      isOpen={togglePopOverComment}
      positions={['bottom', 'top', 'left', 'right']} // preferred positions by priority
      content={() => {
        return (
          <>
            {selectedTask === task.id && (
              <PopOverComment
                task={task}
                dataComments={dataComments}
                dispatchComments={dispatchComments}
                dataUsers={dataUsers}
                dispatchTasks={dispatchTasks}
                commentInTask={commentInTask}
              />
            )}
          </>
        );
      }}
    >
      <span
        className='icon-task icon-comment mdi mdi-comment'
        onClick={handlePopOverComment}
      >
        <Count countValue={commentInTask} />
      </span>
    </Popover>
  );
};

export default ShowComment;
