import { ComponentStory, ComponentMeta } from '@storybook/react';
import ShowComment from './Comment';
import 'containers/Task/task.scss';

export default {
  title: 'ShowComment',
  component: ShowComment,
} as ComponentMeta<typeof ShowComment>;

const Template: ComponentStory<typeof ShowComment> = (args) => (
  <ShowComment {...args} />
);

const IconShowComment = Template.bind({});
IconShowComment.args = {
  togglePopOverComment: true,
  dataComments: [
    {
      id: 1,
      comment:
        'Quo quas perspiciatis sit. Exercitationem molestias voluptas est magni nesciunt. In voluptatem omnis dolores ipsam laborum animi quaerat quod. Temporibus porro sapiente corporis inventore.',
      time: '2022-02-06',
      userId: 1,
    },
    {
      id: 2,
      comment:
        'Libero corporis consequatur qui id et quis mollitia veniam. Ullam quas repellendus repudiandae.',
      time: '2022-02-07',
      userId: 2,
    },
    {
      id: 3,
      comment: 'Alias impedit earum quasi. Tempore corrupti et molestias.',
      time: '2022-06-07',
      userId: 3,
    },
    {
      id: 4,
      comment:
        'Exercitationem commodi accusantium sed quasi est aut. Doloremque sint voluptas quam quam non dignissimos consequatur doloribus. Quae quos quas inventore dolor. Qui fugit quidem laboriosam veniam et.',
      time: '2022-06-12',
      userId: 4,
    },
    {
      id: 5,
      comment:
        'Nihil debitis qui. Numquam laboriosam eveniet ab asperiores. Est necessitatibus qui.',
      time: '2022-07-24',
      userId: 5,
    },
    {
      id: 6,
      comment:
        'Saepe ratione quam qui ipsum animi. Aspernatur ea sunt provident quia ipsa ut accusamus. Fugit eaque veritatis nulla fugit laborum. Consectetur soluta rem.',
      time: '2022-06-03',
      userId: 6,
    },
    {
      id: 7,
      comment:
        'Qui cupiditate voluptatem placeat eos magnam. Quia est aut nemo. Vel consequatur velit. Perferendis eos rerum qui impedit nemo nobis.',
      time: '2022-07-14',
      userId: 7,
    },
    {
      id: 8,
      comment:
        'Accusamus ut quidem dolor a. Magnam eos dolorum et et voluptate voluptatibus et.',
      time: '2022-03-04',
      userId: 8,
    },
    {
      id: 9,
      comment: 'Reiciendis ut ut sed et quam.',
      time: '2022-07-27',
      userId: 9,
    },
    {
      id: 10,
      comment:
        'Molestiae voluptatem fugiat. Qui deleniti nihil autem optio voluptatem aut explicabo esse. Nihil quia molestias molestias occaecati aut ex dolore rerum rem. Aperiam perspiciatis et doloribus consequatur debitis eaque omnis necessitatibus.',
      time: '2022-02-23',
      userId: 10,
    },
  ],
  dataUsers: [
    {
      id: 1,
      name: 'Jayce Crist',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/851.jpg',
      isChecked: true,
    },
    {
      id: 2,
      name: 'Dahlia Gulgowski',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/922.jpg',
      isChecked: true,
    },
    {
      id: 3,
      name: 'Lacey Feeneys',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1192.jpg',
      isChecked: true,
    },
    {
      id: 4,
      name: 'Sasha Kovacek',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1227.jpg',
      isChecked: false,
    },
    {
      id: 5,
      name: 'Lou Collins',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/235.jpg',
      isChecked: false,
    },
    {
      id: 6,
      name: 'Merl McClure',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/422.jpg',
      isChecked: true,
    },
    {
      id: 7,
      name: 'Roosevelt Trantow',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/228.jpg',
      isChecked: false,
    },
    {
      id: 8,
      name: 'Elyse Schuppe',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/231.jpg',
      isChecked: false,
    },
    {
      id: 9,
      name: 'Stone Brakus',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1006.jpg',
      isChecked: true,
    },
    {
      id: 10,
      name: 'Lelah Dickinson',
      avatar:
        'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/134.jpg',
      isChecked: true,
    },
  ],
  task: {
    id: 3,
    title: 'Legacy Identity Producer',
    tagId: [6],
    userId: 3,
    colorId: 3,
    statusId: 3,
    fileId: [7],
    commentId: [1, 4],
  },
  commentInTask: [
    {
      id: 1,
      comment:
        'Quo quas perspiciatis sit. Exercitationem molestias voluptas est magni nesciunt. In voluptatem omnis dolores ipsam laborum animi quaerat quod. Temporibus porro sapiente corporis inventore.',
      time: '2022-02-06',
      userId: 1,
    },
    {
      id: 4,
      comment:
        'Exercitationem commodi accusantium sed quasi est aut. Doloremque sint voluptas quam quam non dignissimos consequatur doloribus. Quae quos quas inventore dolor. Qui fugit quidem laboriosam veniam et.',
      time: '2022-06-12',
      userId: 4,
    },
  ],
  selectedTask: 3,
};

export { IconShowComment };
