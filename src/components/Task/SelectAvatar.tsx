// Lib.
import { Popover } from 'react-tiny-popover';

// Interface.
import { IUser } from 'types/user';
import { IDispatch } from 'types/common';
import { ITask } from 'types/task';

// Container.
import PopOverSelectAvatar from 'components/PopOver/SelectAvatar';

// Component.
import Avatar from 'components/Avatar';

interface IProps {
  popUpAvatarUser: boolean;
  dataUsers: IUser[];
  dispatchTasks: IDispatch<ITask>;
  task: ITask;
  userInTask: IUser;
  onUserInTask: (userInTask: IUser) => void;
  onPopupAvatarUser: (popUpAvatarUser: boolean) => void;
  handlePopOverAvatarUser: () => void;
  selectedTask: number;
}

const ShowSelectAvatar = ({
  popUpAvatarUser,
  dataUsers,
  dispatchTasks,
  task,
  userInTask,
  onUserInTask,
  onPopupAvatarUser,
  handlePopOverAvatarUser,
  selectedTask,
}: IProps) => {
  return (
    <Popover
      isOpen={popUpAvatarUser}
      positions={['bottom', 'top', 'left', 'right']} // preferred positions by priority
      content={() => {
        return (
          <>
            {selectedTask === task.id && (
              <PopOverSelectAvatar
                dataUsers={dataUsers}
                dispatchTasks={dispatchTasks}
                task={task}
                userInTask={userInTask}
                onUserInTask={onUserInTask}
                onPopupAvatarUser={onPopupAvatarUser}
              />
            )}
          </>
        );
      }}
    >
      <div className='task-header-avatar' onClick={handlePopOverAvatarUser}>
        <Avatar size={false} user={userInTask} styleChecked={true} />
      </div>
    </Popover>
  );
};

export default ShowSelectAvatar;
