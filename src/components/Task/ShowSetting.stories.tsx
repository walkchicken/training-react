import { ComponentStory, ComponentMeta } from '@storybook/react';
import ShowSetting from './Setting';
import 'containers/Task/task.scss';

export default {
  title: 'ShowSetting',
  component: ShowSetting,
} as ComponentMeta<typeof ShowSetting>;

const Template: ComponentStory<typeof ShowSetting> = (args) => (
  <ShowSetting {...args} />
);

const IconShowSetting = Template.bind({});
IconShowSetting.args = {
  togglePopOverSetting: true,
  task: {
    id: 3,
    title: 'Legacy Identity Producer',
    tagId: [6],
    userId: 3,
    colorId: 3,
    statusId: 3,
    fileId: [7],
    commentId: [1, 4],
  },
  userInTask: {
    id: 2,
    name: 'Dahlia Gulgowski',
    avatar:
      'https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/922.jpg',
    isChecked: true,
  },
  selectedTask: 3,
};

export { IconShowSetting };
