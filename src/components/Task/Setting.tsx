// Lib.
import { Popover } from 'react-tiny-popover';

// Interface.
import { IUser } from 'types/user';
import { ICheckedInfo, IDispatch, IResponse } from 'types/common';
import { IEditTask, IFile, ITask } from 'types/task';
import { IList } from 'types/list';

// Container.
import PopOverSetting from 'components/PopOver/Setting';

interface IProps {
  togglePopOverSetting: boolean;
  task: ITask;
  column: IList;
  userInTask: IUser;
  colorTask: ICheckedInfo | undefined;
  statusTask: ICheckedInfo | undefined;
  fileInTask: IFile[] | undefined;
  dispatchLists: IDispatch<IList>;
  dataLists: IResponse<IList>;
  dispatchTasks: IDispatch<ITask>;
  dataTasks: IResponse<ITask>;
  handleEdit: (isShow: boolean, task?: IEditTask) => void;
  setTogglePopOverSetting: (togglePopOverSetting: boolean) => void;
  handlePopOverSetting: () => void;
  dataUsers: IResponse<IUser>;
  dataColors: IResponse<ICheckedInfo>;
  dataStatuses: IResponse<ICheckedInfo>;
  selectedTask: number;
}

const ShowSetting = ({
  togglePopOverSetting,
  task,
  column,
  userInTask,
  colorTask,
  statusTask,
  fileInTask,
  dispatchLists,
  dataLists,
  dispatchTasks,
  dataTasks,
  handleEdit,
  setTogglePopOverSetting,
  handlePopOverSetting,
  dataUsers,
  dataColors,
  dataStatuses,
  selectedTask,
}: IProps) => {
  return (
    <Popover
      isOpen={togglePopOverSetting}
      positions={['bottom', 'top', 'left', 'right']} // preferred positions by priority
      content={() => {
        return (
          <>
            {selectedTask === task.id && (
              <PopOverSetting
                task={task}
                column={column}
                userInTask={userInTask}
                colorTask={colorTask}
                statusTask={statusTask}
                fileInTask={fileInTask}
                dispatchLists={dispatchLists}
                dataLists={dataLists}
                dispatchTasks={dispatchTasks}
                dataTasks={dataTasks}
                handleEdit={handleEdit}
                setTogglePopOverSetting={setTogglePopOverSetting}
                dataUsers={dataUsers}
                dataColors={dataColors}
                dataStatuses={dataStatuses}
              />
            )}
          </>
        );
      }}
    >
      <span
        className='icon-task icon-cogs mdi mdi-cogs'
        onClick={handlePopOverSetting}
      />
    </Popover>
  );
};

export default ShowSetting;
