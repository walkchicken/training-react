import { client } from 'helpers/service';
import { IList } from 'types/list';
import { ITask } from 'types/task';

export const updateOnlyList = async (
  url: string,
  sourceId: number,
  sourceColumn: IList,
  copiedItems: number[],
) => {
  return await client.put(`${url}/${sourceId}`, {
    name: sourceColumn.name,
    taskId: copiedItems,
  });
};

export const updateBacklog = async (
  url: string,
  listBacklog: IList,
  taskDefault: ITask,
) => {
  return await client.put(`${url}/${1}`, {
    name: listBacklog.name,
    taskId: [...listBacklog.taskId, taskDefault.id],
  });
};

export const removeTaskInList = async (
  url: string,
  column: IList,
  newTaskId: number[],
) => {
  return await client.put(`${url}/${column.id}`, {
    name: column.name,
    taskId: newTaskId,
  });
};

export const addTaskInList = async (
  url: string,
  column: IList,
  task: ITask,
) => {
  return await client.put(`${url}/${column.id}`, {
    name: column.name,
    taskId: [...column.taskId, task.id],
  });
};
