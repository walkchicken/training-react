import { client } from 'helpers/service';

export const addNewFile = async (url: string, file: File, index: number) => {
  return await client.post(url, {
    id: file.lastModified + index,
    file: file.name,
  });
};
