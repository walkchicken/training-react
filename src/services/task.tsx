import { client } from 'helpers/service';
import { IComment, ITask } from 'types/task';

export const updateTask = async (url: string, data: ITask) => {
  return await client.put(`${url}/${data.id}`, {
    title: data.title,
    tagId: data.tagId,
    userId: data.userId,
    colorId: data.colorId,
    statusId: data.statusId,
    fileId: data.fileId,
    commentId: data.commentId,
  });
};

export const deleteTask = async (url: string, taskIdRemove: number) => {
  return await client.delete(`${url}/${taskIdRemove}`);
};

export const updateComment = async (
  url: string,
  task: ITask,
  comment: IComment,
) => {
  return await client.put(`${url}/${task.id}`, {
    title: task.title,
    tagId: task.tagId,
    userId: task.userId,
    colorId: task.colorId,
    statusId: task.statusId,
    fileId: task.fileId,
    commentId: [...task.commentId, comment.id],
  });
};
