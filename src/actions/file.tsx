// Constant.
import { URL } from 'constants/urls';

// Interface.
import { IFile } from 'types/task';

// Helper.
import { client } from 'helpers/service';
import { addNewFileWithId } from 'actions/createData';

/**
 * Handle add file in task.
 * @param dispatchFiles IDispatch<IFile>.
 * @param typeAdding string.
 * @param typeAddSuccess string.
 * @param typeAddError string.
 * @param data File.
 * @param index number.
 */
export const addNewFile = async (
  dispatchFiles: React.Dispatch<{
    type: string;
    data: IFile[];
  }>,
  typeAdding: string,
  typeAddSuccess: string,
  typeAddError: string,
  data: File,
  index: number,
) => {
  dispatchFiles({ type: typeAdding, data: [] });

  try {
    // Call client from helpers and get data with api from constant url.
    const result = await addNewFileWithId(URL.API_BASE_FILES, data, index);

    dispatchFiles({ type: typeAddSuccess, data: [result.data] });
  } catch (error) {
    dispatchFiles({ type: typeAddError, data: [] });
  }
};

/**
 * Handle get all file.
 * @param dispatchFiles IDispatch<IFile>.
 * @param typeFetching string.
 * @param typeSuccess string.
 * @param typeError string.
 */
export const getDataAllFile = async (
  dispatchFiles: React.Dispatch<{
    type: string;
    data: IFile[];
  }>,
  typeFetching: string,
  typeSuccess: string,
  typeError: string,
) => {
  dispatchFiles({ type: typeFetching, data: [] });

  try {
    // Call client from helpers and get data with api from constant url.
    const result = await client.get(URL.API_BASE_FILES);

    dispatchFiles({ type: typeSuccess, data: result.data });
  } catch (error) {
    dispatchFiles({ type: typeError, data: [] });
  }
};
