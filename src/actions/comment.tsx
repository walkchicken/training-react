// Constant.
import { URL } from 'constants/urls';

// Interface.
import { IComment } from 'types/task';

// Helpers.
import { client } from 'helpers/service';

/**
 * Action handle fetch data all comment task.
 * @param dispatchComments is React.Dispatch of type and data all comment task.
 */
export const getDataAllComment = async (
  dispatchComments: React.Dispatch<{
    type: string;
    data: IComment[];
  }>,
  typeFetching: string,
  typeSuccess: string,
  typeError: string,
) => {
  dispatchComments({ type: typeFetching, data: [] });

  try {
    // Call client from helpers and get data with api from constant url.
    const result = await client.get(URL.API_BASE_COMMENTS);

    dispatchComments({ type: typeSuccess, data: result.data });
  } catch (error) {
    dispatchComments({ type: typeError, data: [] });
  }
};

/**
 * Handle add new comment in task.
 * @param dispatchTasks string.
 * @param typeAdding string.
 * @param typeAddSuccess string.
 * @param typeAddError string.
 * @param data IComment.
 */
export const addNewComment = async (
  dispatchTasks: React.Dispatch<{
    type: string;
    data: IComment[];
  }>,
  typeAdding: string,
  typeAddSuccess: string,
  typeAddError: string,
  data: IComment,
) => {
  dispatchTasks({ type: typeAdding, data: [] });

  try {
    // Call client from helpers and get data with api from constant url.
    const result = await client.post(URL.API_BASE_COMMENTS, data);

    dispatchTasks({ type: typeAddSuccess, data: [result.data] });
  } catch (error) {
    dispatchTasks({ type: typeAddError, data: [] });
  }
};
