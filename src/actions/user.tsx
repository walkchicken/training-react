// Helpers.
import { client } from 'helpers/service';

// Constant.
import { URL } from 'constants/urls';

// Interface.
import { IUser } from 'types/user';

/**
 * Action handle fetch data users.
 * @param dispatchUsers is React.Dispatch of type and data user.
 */
export const getDataUser = async (
  dispatchUsers: React.Dispatch<{
    type: string;
    data: IUser[];
  }>,
  typeFetching: string,
  typeSuccess: string,
  typeError: string,
) => {
  dispatchUsers({ type: typeFetching, data: [] });

  try {
    // Call client from helpers and get data with api from constant url.
    const result = await client.get(URL.API_BASE_USERS);

    dispatchUsers({ type: typeSuccess, data: result.data });
  } catch (error) {
    dispatchUsers({ type: typeError, data: [] });
  }
};
