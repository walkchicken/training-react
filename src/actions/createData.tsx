// Constant.
import { MESSAGE } from 'constants/response';

// Service.
import { addNewFile } from 'services/file';
import { addTaskInList } from 'services/list';

// Interface.
import { IList } from 'types/list';
import { IFile, ITask } from 'types/task';

export const addTaskInListById = async (
  url: string,
  column: IList,
  task: ITask,
) => {
  try {
    // Call api to put data by id.
    const result = await addTaskInList(url, column, task);

    return { data: result.data, message: MESSAGE.SUCCESS };
  } catch (error) {
    return { data: [] as IList[], message: MESSAGE.ERROR };
  }
};

export const addNewFileWithId = async (
  url: string,
  file: File,
  index: number,
) => {
  try {
    // Call api to put data by id.
    const result = await addNewFile(url, file, index);

    return { data: result.data, message: MESSAGE.SUCCESS };
  } catch (error) {
    return { data: [] as IFile[], message: MESSAGE.ERROR };
  }
};
