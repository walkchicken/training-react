// Helpers.
import { client } from 'helpers/service';

// Constant.
import { URL } from 'constants/urls';

// Interface.
import { ICheckedInfo } from 'types/common';

/**
 * Action handle fetch data all status task
 * @param dispatchStatuses is React.Dispatch of type and data all status task
 */
export const getDataAllStatus = async (
  dispatchStatuses: React.Dispatch<{
    type: string;
    data: ICheckedInfo[];
  }>,
  typeFetching: string,
  typeSuccess: string,
  typeError: string,
) => {
  dispatchStatuses({ type: typeFetching, data: [] });

  try {
    // Call client from helpers and get data with api from constant url
    const result = await client.get(URL.API_BASE_STATUSES);

    dispatchStatuses({ type: typeSuccess, data: result.data });
  } catch (error) {
    dispatchStatuses({ type: typeError, data: [] });
  }
};
