// Lib.
import { DraggableLocation } from 'react-beautiful-dnd';

// Constant.
import { URL } from 'constants/urls';

// Interface.
import { IList } from 'types/list';
import { ITask } from 'types/task';

// Helpers.
import { client } from 'helpers/service';
import { removeTaskInListById } from 'actions/removeData';
import {
  updateDataOnlyList,
  updateBacklogListById,
  updateDataAllList,
} from 'actions/updateData';
import { addTaskInListById } from 'actions/createData';

/**
 * Action handle fetch data all list.
 * @param dispatchLists is React.Dispatch of type and data all list.
 */
export const getDataAllList = async (
  dispatchLists: React.Dispatch<{
    type: string;
    data: IList[];
  }>,
  typeFetching: string,
  typeSuccess: string,
  typeError: string,
) => {
  dispatchLists({ type: typeFetching, data: [] });

  try {
    // Call client from helpers and get data with api from constant url.
    const result = await client.get(URL.API_BASE_LISTS);

    dispatchLists({ type: typeSuccess, data: result.data });
  } catch (error) {
    dispatchLists({ type: typeError, data: [] });
  }
};

/**
 * Handle update Backlog list when create new task.
 * @param dispatchLists IDispatch<IList>.
 * @param typeUpdatingBacklog string.
 * @param typeUpdateBacklogSuccess string.
 * @param typeUpdateBacklogError string.
 * @param listBacklog IList.
 * @param valueNewTask ITask.
 */
export const updateBacklogList = async (
  dispatchLists: React.Dispatch<{
    type: string;
    data: IList[];
  }>,
  typeUpdatingBacklog: string,
  typeUpdateBacklogSuccess: string,
  typeUpdateBacklogError: string,
  listBacklog: IList,
  valueNewTask: ITask,
) => {
  dispatchLists({ type: typeUpdatingBacklog, data: [] });

  // Call api api from service with url and data.
  const result = await updateBacklogListById(
    URL.API_BASE_LISTS,
    listBacklog,
    valueNewTask,
  );

  if (!result) {
    dispatchLists({ type: typeUpdateBacklogError, data: [] });
  }

  dispatchLists({ type: typeUpdateBacklogSuccess, data: [result.data] });
};

/**
 * Handle update all list when change drag drop.
 * @param dispatchLists IDispatch<IList>.
 * @param typeUpdating string.
 * @param typeUpdateListSuccess string.
 * @param typeUpdateListError string.
 * @param columns IList array.
 * @param source DraggableLocation.
 * @param sourceColumn IList source list.
 * @param sourceItems number[] taskId.
 * @param destination DraggableLocation.
 * @param destColumn IList destination list.
 * @param destItems number[] taskId.
 */
export const updateAllList = async (
  dispatchLists: React.Dispatch<{
    type: string;
    data: IList[];
  }>,
  typeUpdating: string,
  typeUpdateListSuccess: string,
  typeUpdateListError: string,
  columns: IList[],
  source: DraggableLocation,
  sourceColumn: IList,
  sourceItems: number[],
  destination: DraggableLocation,
  destColumn: IList,
  destItems: number[],
) => {
  dispatchLists({ type: typeUpdating, data: columns });

  // Call api api from service with url and data.
  const result = await updateDataAllList(
    URL.API_BASE_LISTS,
    columns,
    source,
    sourceColumn,
    sourceItems,
    destination,
    destColumn,
    destItems,
  );

  if (!result) {
    dispatchLists({ type: typeUpdateListError, data: columns });
  }

  dispatchLists({ type: typeUpdateListSuccess, data: result.data });
};

/**
 * Handle update only list change when drag drop task.
 * @param dispatchLists IDispatch<IList>.
 * @param typeUpdatingOnlyList string.
 * @param typeUpdateOnlyListSuccess string.
 * @param typeUpdateOnlyListError string.
 * @param columns IList array.
 * @param source DraggableLocation.
 * @param sourceColumn IList.
 * @param copiedItems number[] taskId.
 */
export const updateOnlyList = async (
  dispatchLists: React.Dispatch<{
    type: string;
    data: IList[];
  }>,
  typeUpdatingOnlyList: string,
  typeUpdateOnlyListSuccess: string,
  typeUpdateOnlyListError: string,
  columns: IList[],
  source: DraggableLocation,
  sourceColumn: IList,
  copiedItems: number[],
) => {
  dispatchLists({ type: typeUpdatingOnlyList, data: columns });

  // Call api api from service with url and data.
  const result = await updateDataOnlyList(
    URL.API_BASE_LISTS,
    columns,
    source,
    sourceColumn,
    copiedItems,
  );

  if (!result) {
    dispatchLists({ type: typeUpdateOnlyListError, data: columns });
  }

  dispatchLists({ type: typeUpdateOnlyListSuccess, data: result.data });
};

/**
 * Handle update list after remove task in it.
 * @param dispatchLists IDispatch<IList>.
 * @param typeRemoving string.
 * @param typeRemoveSuccess string.
 * @param typeRemoveError string.
 * @param dataLists IList array.
 * @param column IList
 * @param taskIdRemove number.
 */
export const updateListAfterRemoveTask = async (
  dispatchLists: React.Dispatch<{
    type: string;
    data: IList[];
  }>,
  typeRemoving: string,
  typeRemoveSuccess: string,
  typeRemoveError: string,
  dataLists: IList[],
  column: IList,
  taskIdRemove: number,
) => {
  dispatchLists({ type: typeRemoving, data: [] });

  // Call api api from service with url and data.
  const result = await removeTaskInListById(
    URL.API_BASE_LISTS,
    dataLists,
    column,
    taskIdRemove,
  );

  if (!result) {
    dispatchLists({ type: typeRemoveError, data: [] });
  }

  dispatchLists({ type: typeRemoveSuccess, data: result.data });
};

/**
 * Handle update list with task copy.
 * @param dispatchLists IDispatch<IList>.
 * @param typeUpdatingAfterAddTask string.
 * @param typeUpdateAfterAddTaskSuccess string.
 * @param typeUpdateAfterAddTaskError string.
 * @param column IList.
 * @param valueTaskCopy ITask.
 */
export const updateListAfterAddTask = async (
  dispatchLists: React.Dispatch<{
    type: string;
    data: IList[];
  }>,
  typeUpdatingAfterAddTask: string,
  typeUpdateAfterAddTaskSuccess: string,
  typeUpdateAfterAddTaskError: string,
  column: IList,
  valueTaskCopy: ITask,
) => {
  dispatchLists({ type: typeUpdatingAfterAddTask, data: [] });

  // Call api api from service with url and data.
  const result = await addTaskInListById(
    URL.API_BASE_LISTS,
    column,
    valueTaskCopy,
  );

  if (!result) {
    dispatchLists({ type: typeUpdateAfterAddTaskError, data: [] });
  }

  dispatchLists({ type: typeUpdateAfterAddTaskSuccess, data: [result.data] });
};
