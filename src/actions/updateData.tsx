// Lib.
import { DraggableLocation } from 'react-beautiful-dnd';

// Constant.
import { MESSAGE } from 'constants/response';

// Service.
import { updateBacklog, updateOnlyList } from 'services/list';
import { updateComment, updateTask } from 'services/task';

// Interface.
import { IList } from 'types/list';
import { IComment, ITask } from 'types/task';

// Helper.
import { formatDataAllList, formatDataOnlyList } from 'helpers/lits';

export const updateDataOnlyList = async (
  url: string,
  columns: IList[],
  source: DraggableLocation,
  sourceColumn: IList,
  copiedItems: number[],
) => {
  try {
    // Call api to put data by id.
    const sourceId = Number(source.droppableId) + 1;
    const sourceResult = await updateOnlyList(
      url,
      sourceId,
      sourceColumn,
      copiedItems,
    );
    const currentData = formatDataOnlyList(columns, sourceResult);

    return { data: currentData, message: MESSAGE.SUCCESS };
  } catch (error) {
    return { data: [] as IList[], message: MESSAGE.ERROR };
  }
};

export const updateDataAllList = async (
  url: string,
  columns: IList[],
  source: DraggableLocation,
  sourceColumn: IList,
  sourceItems: number[],
  destination: DraggableLocation,
  destColumn: IList,
  destItems: number[],
) => {
  try {
    // Call api to put data by id.
    const sourceId = Number(source.droppableId) + 1;
    const destinationId = Number(destination.droppableId) + 1;

    // Handle update list source droppable.
    const sourceResult = await updateOnlyList(
      url,
      sourceId,
      sourceColumn,
      sourceItems,
    );

    // Handle update list destination droppable.
    const destinationResult = await updateOnlyList(
      url,
      destinationId,
      destColumn,
      destItems,
    );

    // Format data list array.
    const currentData = formatDataAllList(
      columns,
      sourceResult,
      destinationResult,
    );

    return { data: currentData, message: MESSAGE.SUCCESS };
  } catch (error) {
    return { data: [] as IList[], message: MESSAGE.ERROR };
  }
};

/**
 * Update list backlog by data new task.
 * @param url string for action.
 * @param listBacklog data IList backlog.
 * @param taskDefault data ITask.
 * @returns new task in list back log or message error.
 */
export const updateBacklogListById = async (
  url: string,
  listBacklog: IList,
  taskDefault: ITask,
) => {
  try {
    // Call api to put data by id.
    const result = await updateBacklog(url, listBacklog, taskDefault);

    return { data: result.data, message: MESSAGE.SUCCESS };
  } catch (error) {
    return { data: [] as IList[], message: MESSAGE.ERROR };
  }
};

/**
 * Update task by id.
 * @param url string from action.
 * @param data ITask from action.
 * @returns data or message error.
 */
export const updateTaskById = async (url: string, data: ITask) => {
  try {
    // Call to api to update task.
    const result = await updateTask(url, data);

    return { data: result.data, message: MESSAGE.SUCCESS };
  } catch (error) {
    return { data: [] as ITask[], message: MESSAGE.ERROR };
  }
};

export const updateCommentInTask = async (
  url: string,
  task: ITask,
  comment: IComment,
) => {
  try {
    // Call api to put data by id.
    const result = await updateComment(url, task, comment);

    return { data: result.data, message: MESSAGE.SUCCESS };
  } catch (error) {
    return { data: [] as ITask[], message: MESSAGE.ERROR };
  }
};
