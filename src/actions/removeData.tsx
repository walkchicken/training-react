// Constant.
import { MESSAGE } from 'constants/response';

// Service.
import { removeTaskInList } from 'services/list';
import { deleteTask } from 'services/task';

// Interface.
import { IList } from 'types/list';
import { ITask } from 'types/task';

// Helper.
import { filterIdRemove, filterRemoveTask } from 'helpers/filter';

export const removeTaskInListById = async (
  url: string,
  dataLists: IList[],
  column: IList,
  taskIdRemove: number,
) => {
  try {
    const newTaskId = filterIdRemove(column, taskIdRemove);

    // Call api to put data by id.
    const result = await removeTaskInList(url, column, newTaskId);

    const idxSource = dataLists.findIndex((obj) => obj.id === column.id);

    let currentData: IList[] = [];
    // current data.
    currentData = [...dataLists];

    if (idxSource >= 0) {
      currentData[idxSource].taskId = result.data.taskId;
    }

    return { data: currentData, message: MESSAGE.SUCCESS };
  } catch (error) {
    return { data: [] as IList[], message: MESSAGE.ERROR };
  }
};

export const deleteTaskById = async (
  url: string,
  dataTasks: ITask[],
  taskIdRemove: number,
) => {
  try {
    // Call to api to update task.
    const result = await deleteTask(url, taskIdRemove);

    const newListTask = filterRemoveTask(dataTasks, taskIdRemove);

    return { data: newListTask, message: MESSAGE.SUCCESS };
  } catch (error) {
    return { data: [] as ITask[], message: MESSAGE.ERROR };
  }
};
