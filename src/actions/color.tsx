// Helpers.
import { client } from 'helpers/service';

// Constant.
import { URL } from 'constants/urls';

// Interface.
import { ICheckedInfo } from 'types/common';

/**
 * Action handle fetch data all color task.
 * @param dispatchColorTask is React.Dispatch of type and data all color task.
 */
export const getDataAllColorTask = async (
  dispatchColorTask: React.Dispatch<{
    type: string;
    data: ICheckedInfo[];
  }>,
  typeFetching: string,
  typeSuccess: string,
  typeError: string,
) => {
  dispatchColorTask({ type: typeFetching, data: [] });

  try {
    // Call client from helpers and get data with api from constant url.
    const result = await client.get(URL.API_BASE_COLORS);

    dispatchColorTask({ type: typeSuccess, data: result.data });
  } catch (error) {
    dispatchColorTask({ type: typeError, data: [] });
  }
};
