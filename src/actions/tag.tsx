// Helpers.
import { client } from 'helpers/service';

// Constant.
import { URL } from 'constants/urls';

// Interface.
import { ICheckedInfo } from 'types/common';

/**
 * Action handle fetch data tags.
 * @param dispatchTags is React.Dispatch of type and data tag.
 */
export const getDataAllTag = async (
  dispatchTags: React.Dispatch<{
    type: string;
    data: ICheckedInfo[];
  }>,
  typeFetching: string,
  typeSuccess: string,
  typeError: string,
) => {
  dispatchTags({ type: typeFetching, data: [] });

  try {
    // Call client from helpers and get data with api from constant url.
    const result = await client.get(URL.API_BASE_TAGS);

    dispatchTags({ type: typeSuccess, data: result.data });
  } catch (error) {
    dispatchTags({ type: typeError, data: [] });
  }
};
