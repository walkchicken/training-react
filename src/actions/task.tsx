// Helpers.
import { client } from 'helpers/service';

// Constant..
import { URL } from 'constants/urls';

// Interface..
import { IComment, ITask } from 'types/task';
import { updateCommentInTask, updateTaskById } from 'actions/updateData';
import { deleteTaskById } from 'actions/removeData';

/**
 * Action handle fetch data all list.
 * @param dispatchTasks is React.Dispatch of type and data all list.
 */
export const getDataAllTask = async (
  dispatchTasks: React.Dispatch<{
    type: string;
    data: ITask[];
  }>,
  typeFetching: string,
  typeSuccess: string,
  typeError: string,
) => {
  dispatchTasks({ type: typeFetching, data: [] });

  try {
    // Call client from helpers and get data with api from constant url.
    const result = await client.get(URL.API_BASE_TASKS);

    dispatchTasks({ type: typeSuccess, data: result.data });
  } catch (error) {
    dispatchTasks({ type: typeError, data: [] });
  }
};

export const addNewTask = async (
  dispatchTasks: React.Dispatch<{
    type: string;
    data: ITask[];
  }>,
  typeAdding: string,
  typeAddSuccess: string,
  typeAddError: string,
  data: ITask,
) => {
  dispatchTasks({ type: typeAdding, data: [] });

  try {
    // Call client from helpers and get data with api from constant url.
    const result = await client.post(URL.API_BASE_TASKS, data);

    dispatchTasks({ type: typeAddSuccess, data: [result.data] });
  } catch (error) {
    dispatchTasks({ type: typeAddError, data: [] });
  }
};

export const updateTask = async (
  dispatchTasks: React.Dispatch<{
    type: string;
    data: ITask[];
  }>,
  typeUpdating: string,
  typeUpdateSuccess: string,
  typeUpdateError: string,
  data: ITask,
) => {
  dispatchTasks({ type: typeUpdating, data: [] });

  // Call to api from service with url and data.
  const result = await updateTaskById(URL.API_BASE_TASKS, data);

  if (!result) {
    return dispatchTasks({ type: typeUpdateError, data: [] });
  }

  dispatchTasks({ type: typeUpdateSuccess, data: [result.data] });
};

export const deleteTask = async (
  dispatchTasks: React.Dispatch<{
    type: string;
    data: ITask[];
  }>,
  typeRemoving: string,
  typeRemoveSuccess: string,
  typeRemoveError: string,
  dataTasks: ITask[],
  taskIdRemove: number,
) => {
  dispatchTasks({ type: typeRemoving, data: [] });

  // Call to api from service with url and data.
  const result = await deleteTaskById(
    URL.API_BASE_TASKS,
    dataTasks,
    taskIdRemove,
  );

  if (!result) {
    return dispatchTasks({ type: typeRemoveError, data: [] });
  }

  dispatchTasks({ type: typeRemoveSuccess, data: result.data });
};

export const updateTaskAfterAddComment = async (
  dispatchLists: React.Dispatch<{
    type: string;
    data: ITask[];
  }>,
  typeUpdatingAfterAddComment: string,
  typeUpdateAfterAddCommentSuccess: string,
  typeUpdateAfterAddCommentError: string,
  task: ITask,
  valueNewComment: IComment,
) => {
  dispatchLists({ type: typeUpdatingAfterAddComment, data: [] });
  // Call api api from service with url and data.
  const result = await updateCommentInTask(
    URL.API_BASE_TASKS,
    task,
    valueNewComment,
  );

  if (!result) {
    dispatchLists({ type: typeUpdateAfterAddCommentError, data: [] });
  }

  dispatchLists({
    type: typeUpdateAfterAddCommentSuccess,
    data: [result.data],
  });
};
