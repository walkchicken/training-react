// Constant.
import { LABEL_TAG } from 'constants/actions/tag';

export const renderTagById = (item: number) => {
  switch (item) {
    case 1:
      return LABEL_TAG.WEBIX;
    case 2:
      return LABEL_TAG.JET;
    case 3:
      return LABEL_TAG.EASY;
    case 4:
      return LABEL_TAG.HARD;
    case 5:
      return LABEL_TAG.KABAN;
    case 6:
      return LABEL_TAG.DOCS;
    default:
      return '';
  }
};
