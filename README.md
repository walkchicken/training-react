## NHATDUONG REACT TRAINING

Web application for KanBan, that includes:
- Filter user.
- Task management (CURD).
- Setting task.
- Change user by avatar.
- Upload file.
- Drag drop task.
- Comment on task.
- Count comments and files.

## DEVELOPMENT TEAM

- Dev - [Nhat Duong](dcnhat1862000@gmail.com).

## PLAN

- [Internship React Document](https://docs.google.com/document/d/1O1QJ03Xqafz57d4dJ_01t4Hqs0NYdkEqXtjGEgTdJfk/edit).
- [Plan On Document](https://docs.google.com/document/d/13b6-0eDtPQWUJyJxj8ck9X7Jpn44unucF8LWxsQ7Uds/edit).
- [Plan On Gitlab](https://gitlab.com/walkchicken/training-react/-/boards).

## INFORMATION:

- Timeline:
    - Estimate time: 14 days.
    - Actual time: 25 days.

- Editor: Visual Studio Code.
- Responsive: Not support responsive.

##  TARGETS

- Apply Storybook.
- Build an application using React.
- Reusable components.
- Apply React/ React hooks.
- Use Context, Use Reducer of React Hook to state management.
- Check re-render and avoid re-render.

## TECHNICAL STACKS

- HTML5.
- CSS3.
- Sass.
- [Vite v3.0.0](https://vitejs.dev/guide/#scaffolding-your-first-vite-project).
- [React](https://reactjs.org/docs/introducing-jsx.html).
- [Json-server](https://www.npmjs.com/package/json-server).

## ENVIRONMENT

- Node.js v14.18.1.
- Yarn v1.22.17

## DEPENDENCIES

- @mdi/font v7.0.96.
- axios v0.27.2.
- clsx v1.2.1.
- react v18.2.0.
- react-beautiful-dnd v13.1.0.
- react-dom v18.2.0.
- react-drag-drop-files v2.3.7.
- react-select v5.4.0.
- react-tiny-popover v7.1.0.

## REFERENCES

- [Design of KanBan](https://webix.com/demos/kanban/)

## WORKFLOW

![Work flow in diagram][workflow]

[workflow]: src/assets/images/workflow.png


## SETUP & RUN

- Install Yarn through the [npm package manager](https://www.npmjs.com/), which comes bundled with [Node.js](https://nodejs.org/en/) when you install it on your system.
- Once you have npm installed you can run the following both to install and upgrade Yarn:

```bash
npm install --global yarn
```
- Setup and run project:

```bash
git clone https://gitlab.com/walkchicken/training-react.git
cd training-react
yarn install
yarn run build
yarn run dev
output: http://127.0.0.1:5173/
```
